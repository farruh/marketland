# from account.conf import AccountAppConf
from django.apps import AppConfig


# class AccountConfig(AccountAppConf):
class AccountConfig(AppConfig):
    name = 'account'
