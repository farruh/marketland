from collections import OrderedDict
from crispy_forms.bootstrap import FormActions, InlineCheckboxes, Accordion, AccordionGroup, Tab, Field, TabHolder
from django import forms
from django.forms import ModelForm, Textarea, CheckboxSelectMultiple, MultipleChoiceField, Form, CharField, DateField, \
    inlineformset_factory
from customer.models import Company, Employee, Product, Order, ProductImage, CompanyFile
from account.conf import settings
from django.utils.translation import ugettext_lazy as _

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout, Fieldset, ButtonHolder, HTML, Div, Button, MultiField
from mtplus.fields import MultiFileField, MultiImageField, DefaultFileInput, DefaultImageInput, CharMaskField
from mtplus.form_utils import get_helper_hr_tab_form
from mtplus.widgets import BootstrapDateWidget


class BaseForm(ModelForm):
    class Meta:
        abstract = True
        fields = ('__all__')


class CompanyForm(BaseForm):
    foundation_date = DateField(widget=BootstrapDateWidget)
    inn = CharMaskField(max_length=10, min_length=9, mask='9999999999')
    okonx = CharMaskField(max_length=5, min_length=5, mask='99999')
    mfo = CharMaskField(max_length=5, min_length=5, mask='99999')
    bank_account_number = CharMaskField(max_length=20, min_length=20, mask='99999999999999999999')

    class Meta:
        model = Company
        fields = ('__all__')
        widgets = {
            'logo': DefaultImageInput,
            'notes': Textarea(attrs={'colls': 50, 'rows': 15})

        }

    def __init__(self, *args, **kwargs):
        super(CompanyForm, self).__init__(*args, **kwargs)
        fields = OrderedDict()
        fields['Info'] = ['name', 'logo', 'company_type', 'address', 'country', 'website', 'email', 'phone',
                          'foundation_date',
                          'employees_amount', 'production_power', 'notes']
        fields['Account'] = ['inn', 'bank_name', 'bank_account_number', 'mfo', 'okonx']
        fields['Files'] = ['$$html.{% include "customer/inline_formset.html" %}']
        self.helper = get_helper_hr_tab_form('companyForm', fields)
        self.helper.form_tag = False


CompanyFormSet = inlineformset_factory(Company, CompanyFile, fields=('file',), widgets={'file': DefaultFileInput})


class EmployeeForm(ModelForm):
    class Meta:
        model = Employee
        fields = ('__all__')


class ProductForm(ModelForm):
    # images = MultiImageField(label=_('Images'), min_num=1, max_num=3, max_file_size=1024 * 1024 * 5)
    # images = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': True}))
    # images = forms.FileField()
    class Meta:
        model = Product
        fields = ('__all__')
        # exclude=['company']
        widgets = {
            'notes': Textarea(attrs={'colls': 50, 'rows': 15}),
            # 'images': forms.ClearableFileInput(attrs={'multiple': True})
        }

    def save(self, commit=True):
        return super().save(commit)


    def __init__(self, *args, **kwargs):
        super(ProductForm, self).__init__(*args, **kwargs)
        fields = OrderedDict()
        fields['Info'] = ['name', 'company', 'category',  'notes']
        fields['Data'] = ['price', 'currency', 'amount', 'unit', 'power', 'min_order', 'status']
        fields['Images'] = ['$$html.{% include "customer/inline_formset.html" %}']
        self.helper = get_helper_hr_tab_form('productForm', fields)
        self.helper.form_tag = False
        # self.helper['images'].wrap(HTML(' '))


ProductFormSet = inlineformset_factory(Product, ProductImage, fields=('image', 'order'),
                                       widgets={'image': DefaultFileInput})


class OrderForm(ModelForm):
    class Meta:
        model = Order
        fields = ('__all__')
        widgets = {
            'notes': Textarea(),
        }
        # exclude=['product']

    def __init__(self, *args, **kwargs):
        super(OrderForm, self).__init__(*args, **kwargs)
        self.helper = get_helper_hr_tab_form(
            'orderForm',
            {
                '1. Info': ['number', 'product', 'customer', 'customer_company', 'order_amount', 'order_amount_unit',
                            'notes', 'status'],
            }
        )


class ProfileForm(ModelForm):
    # user:
    last_name = CharField(label=_("Last name"), max_length=30, required=True)
    first_name = CharField(label=_("First name"), max_length=30, required=True)
    middle_name = CharField(label=_("Middle name"), max_length=40, required=True)

    dob = DateField(label=_('Date of Birth'), required=True, widget=BootstrapDateWidget)
    # sex = CharField(label=_('Sex'), required=True)

    # address
    phone = CharField(label=_("Phone"), required=True)


    # employee
    # company_name = CharField(label=_('Company name'), max_length=50, required=True)
    # company 
    # job_position = CharField(label=_('Job position'))
    # certificates = CharField(label=_('Certificates'), max_length=255, required=True)


    # account:
    # employee:


    # company:
    class Meta:
        model = Employee
        fields = ('__all__')
        exclude = ('user',)

    def __init__(self, *args, **kwargs):
        super(ProfileForm, self).__init__(*args, **kwargs)
        fields = OrderedDict()
        fields['Info'] = ['last_name', 'first_name', 'middle_name', 'sex', 'dob', 'phone']
        fields['Company'] = ['company', 'job_position', 'certificates']
        self.helper = get_helper_hr_tab_form('profileForm', fields)
