from __future__ import unicode_literals
from django.http import HttpResponseRedirect
from django.views.generic import UpdateView

from django.views.generic.base import TemplateView
from django.contrib import messages


class HomePageView(TemplateView):
    template_name = 'landing/index.html'

    def get_context_data(self, **kwargs):
        context = super(HomePageView, self).get_context_data(**kwargs)
        # messages.info(self.request, 'Info: Hello')
        # messages.warning(self.request, 'Warning: Hello')
        # messages.debug(self.request, 'Debug: Hello')
        # messages.success(self.request, 'Success: Hello')
        return context

class ContactPageView(TemplateView):
    template_name = 'mtplus/contact.html'
    
class FaqPageView(TemplateView):
    template_name = 'mtplus/faq.html'

class FormsetUpdateView(UpdateView):
    formset_class = None

    def get_formset_class(self):
        """
        Returns the formset class to use in this view
        """
        return self.formset_class

    def get_formset(self, formset_class=None):
        """
        Returns an instance of the formset to be used in this view.
        """
        if formset_class is None:
            formset_class = self.get_formset_class()
        return formset_class(**self.get_form_kwargs())

    def get(self, request, *args, **kwargs):
        if self.get_object() is not None:
            self.object = self.get_object()
        form = self.get_form()
        formset = self.get_formset()

        return self.render_to_response(
            self.get_context_data(form=form, formset=formset)
        )

    def post(self, request, *args, **kwargs):
        if self.get_object() is not None:
            self.object = self.get_object()
        form = self.get_form()
        formset = self.get_formset()
        print('form is valid = %s' % form.is_valid)
        print('formset is valid = %s' % formset.is_valid)
        if form.is_valid():
            print('Form is valid')
        if formset.is_valid():
            print("Formset is valid")

        if form.is_valid() and formset.is_valid():
            print('forms and formset is valid   ')
            return self.forms_valid(form, formset)

        return self.forms_invalid(form, formset)

    def forms_valid(self, form, formset):
        """
        Called if all forms are valid. Creates a Author instance along
        with associated books and then redirects to a success page.
        """
        print('valid form')
        self.object = form.save()
        formset.instance = self.object
        savedobjects = formset.save()
        # savedobjects.save()

        return HttpResponseRedirect(self.get_success_url())

    def forms_invalid(self, form, formset):
        """
        Called if whether a form is invalid. Re-renders the context
        data with the data-filled forms and errors.
        """
        return self.render_to_response(
            self.get_context_data(form=form, formset=formset)
        )

    def get_context_data(self, **kwargs):
        """ Add formset and formset to the context_data. """
        ctx = super(FormsetUpdateView, self).get_context_data(**kwargs)

        if self.request.POST:
            ctx['formset'] = self.get_formset()
        else:
            ctx['formset'] = self.get_formset()

        return ctx


# class FormsetCreateView(FormsetUpdateView):
#     def get_object(self, queryset=None):
#         return None