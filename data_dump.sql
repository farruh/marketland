--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.1
-- Dumped by pg_dump version 9.4.1
-- Started on 2017-04-03 13:34:06

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 225 (class 3079 OID 11855)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2382 (class 0 OID 0)
-- Dependencies: 225
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 179 (class 1259 OID 182161)
-- Name: auth_group; Type: TABLE; Schema: public; Owner: invest; Tablespace: 
--

CREATE TABLE auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE auth_group OWNER TO invest;

--
-- TOC entry 178 (class 1259 OID 182159)
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: invest
--

CREATE SEQUENCE auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_group_id_seq OWNER TO invest;

--
-- TOC entry 2383 (class 0 OID 0)
-- Dependencies: 178
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: invest
--

ALTER SEQUENCE auth_group_id_seq OWNED BY auth_group.id;


--
-- TOC entry 181 (class 1259 OID 182171)
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: invest; Tablespace: 
--

CREATE TABLE auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE auth_group_permissions OWNER TO invest;

--
-- TOC entry 180 (class 1259 OID 182169)
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: invest
--

CREATE SEQUENCE auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_group_permissions_id_seq OWNER TO invest;

--
-- TOC entry 2384 (class 0 OID 0)
-- Dependencies: 180
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: invest
--

ALTER SEQUENCE auth_group_permissions_id_seq OWNED BY auth_group_permissions.id;


--
-- TOC entry 177 (class 1259 OID 182153)
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: invest; Tablespace: 
--

CREATE TABLE auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE auth_permission OWNER TO invest;

--
-- TOC entry 176 (class 1259 OID 182151)
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: invest
--

CREATE SEQUENCE auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_permission_id_seq OWNER TO invest;

--
-- TOC entry 2385 (class 0 OID 0)
-- Dependencies: 176
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: invest
--

ALTER SEQUENCE auth_permission_id_seq OWNED BY auth_permission.id;


--
-- TOC entry 183 (class 1259 OID 182179)
-- Name: auth_user; Type: TABLE; Schema: public; Owner: invest; Tablespace: 
--

CREATE TABLE auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE auth_user OWNER TO invest;

--
-- TOC entry 185 (class 1259 OID 182189)
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: invest; Tablespace: 
--

CREATE TABLE auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE auth_user_groups OWNER TO invest;

--
-- TOC entry 184 (class 1259 OID 182187)
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: invest
--

CREATE SEQUENCE auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_groups_id_seq OWNER TO invest;

--
-- TOC entry 2386 (class 0 OID 0)
-- Dependencies: 184
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: invest
--

ALTER SEQUENCE auth_user_groups_id_seq OWNED BY auth_user_groups.id;


--
-- TOC entry 182 (class 1259 OID 182177)
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: invest
--

CREATE SEQUENCE auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_id_seq OWNER TO invest;

--
-- TOC entry 2387 (class 0 OID 0)
-- Dependencies: 182
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: invest
--

ALTER SEQUENCE auth_user_id_seq OWNED BY auth_user.id;


--
-- TOC entry 187 (class 1259 OID 182197)
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: invest; Tablespace: 
--

CREATE TABLE auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE auth_user_user_permissions OWNER TO invest;

--
-- TOC entry 186 (class 1259 OID 182195)
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: invest
--

CREATE SEQUENCE auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_user_permissions_id_seq OWNER TO invest;

--
-- TOC entry 2388 (class 0 OID 0)
-- Dependencies: 186
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: invest
--

ALTER SEQUENCE auth_user_user_permissions_id_seq OWNED BY auth_user_user_permissions.id;


--
-- TOC entry 196 (class 1259 OID 202434)
-- Name: customer_company; Type: TABLE; Schema: public; Owner: invest; Tablespace: 
--

CREATE TABLE customer_company (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    address character varying(200) NOT NULL,
    phone character varying(100) NOT NULL,
    bank_name character varying(100) NOT NULL,
    bank_account_number character varying(100) NOT NULL,
    mfo character varying(100) NOT NULL,
    inn character varying(100) NOT NULL,
    okonx character varying(100) NOT NULL,
    website character varying(100) NOT NULL,
    email character varying(100) NOT NULL,
    company_type character varying(2) NOT NULL,
    foundation_date date NOT NULL,
    employees_amount integer NOT NULL,
    production_power integer NOT NULL,
    notes character varying(1024) NOT NULL,
    created_on timestamp with time zone NOT NULL,
    last_modified timestamp with time zone NOT NULL
);


ALTER TABLE customer_company OWNER TO invest;

--
-- TOC entry 224 (class 1259 OID 207717)
-- Name: customer_company_files; Type: TABLE; Schema: public; Owner: invest; Tablespace: 
--

CREATE TABLE customer_company_files (
    id integer NOT NULL,
    company_id integer NOT NULL,
    file_id integer NOT NULL
);


ALTER TABLE customer_company_files OWNER TO invest;

--
-- TOC entry 223 (class 1259 OID 207715)
-- Name: customer_company_files_id_seq; Type: SEQUENCE; Schema: public; Owner: invest
--

CREATE SEQUENCE customer_company_files_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE customer_company_files_id_seq OWNER TO invest;

--
-- TOC entry 2389 (class 0 OID 0)
-- Dependencies: 223
-- Name: customer_company_files_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: invest
--

ALTER SEQUENCE customer_company_files_id_seq OWNED BY customer_company_files.id;


--
-- TOC entry 195 (class 1259 OID 202432)
-- Name: customer_company_id_seq; Type: SEQUENCE; Schema: public; Owner: invest
--

CREATE SEQUENCE customer_company_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE customer_company_id_seq OWNER TO invest;

--
-- TOC entry 2390 (class 0 OID 0)
-- Dependencies: 195
-- Name: customer_company_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: invest
--

ALTER SEQUENCE customer_company_id_seq OWNED BY customer_company.id;


--
-- TOC entry 198 (class 1259 OID 202445)
-- Name: customer_employee; Type: TABLE; Schema: public; Owner: invest; Tablespace: 
--

CREATE TABLE customer_employee (
    id integer NOT NULL,
    first_name character varying(100) NOT NULL,
    last_name character varying(100) NOT NULL,
    middle_name character varying(100) NOT NULL,
    job_position_id integer,
    sex character varying(100) NOT NULL,
    certificates character varying(255) NOT NULL,
    email character varying(100) NOT NULL,
    phone character varying(100) NOT NULL,
    company_id integer NOT NULL,
    created_on timestamp with time zone NOT NULL,
    last_modified timestamp with time zone NOT NULL
);


ALTER TABLE customer_employee OWNER TO invest;

--
-- TOC entry 197 (class 1259 OID 202443)
-- Name: customer_employee_id_seq; Type: SEQUENCE; Schema: public; Owner: invest
--

CREATE SEQUENCE customer_employee_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE customer_employee_id_seq OWNER TO invest;

--
-- TOC entry 2391 (class 0 OID 0)
-- Dependencies: 197
-- Name: customer_employee_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: invest
--

ALTER SEQUENCE customer_employee_id_seq OWNED BY customer_employee.id;


--
-- TOC entry 204 (class 1259 OID 207481)
-- Name: customer_employeejobposition; Type: TABLE; Schema: public; Owner: invest; Tablespace: 
--

CREATE TABLE customer_employeejobposition (
    id integer NOT NULL,
    created_on timestamp with time zone NOT NULL,
    last_modified timestamp with time zone NOT NULL,
    name character varying(100) NOT NULL
);


ALTER TABLE customer_employeejobposition OWNER TO invest;

--
-- TOC entry 203 (class 1259 OID 207479)
-- Name: customer_employeejobposition_id_seq; Type: SEQUENCE; Schema: public; Owner: invest
--

CREATE SEQUENCE customer_employeejobposition_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE customer_employeejobposition_id_seq OWNER TO invest;

--
-- TOC entry 2392 (class 0 OID 0)
-- Dependencies: 203
-- Name: customer_employeejobposition_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: invest
--

ALTER SEQUENCE customer_employeejobposition_id_seq OWNED BY customer_employeejobposition.id;


--
-- TOC entry 206 (class 1259 OID 207489)
-- Name: customer_file; Type: TABLE; Schema: public; Owner: invest; Tablespace: 
--

CREATE TABLE customer_file (
    id integer NOT NULL,
    created_on timestamp with time zone NOT NULL,
    last_modified timestamp with time zone NOT NULL,
    name character varying(100) NOT NULL,
    file character varying(100) NOT NULL
);


ALTER TABLE customer_file OWNER TO invest;

--
-- TOC entry 205 (class 1259 OID 207487)
-- Name: customer_file_id_seq; Type: SEQUENCE; Schema: public; Owner: invest
--

CREATE SEQUENCE customer_file_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE customer_file_id_seq OWNER TO invest;

--
-- TOC entry 2393 (class 0 OID 0)
-- Dependencies: 205
-- Name: customer_file_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: invest
--

ALTER SEQUENCE customer_file_id_seq OWNED BY customer_file.id;


--
-- TOC entry 208 (class 1259 OID 207497)
-- Name: customer_order; Type: TABLE; Schema: public; Owner: invest; Tablespace: 
--

CREATE TABLE customer_order (
    id integer NOT NULL,
    created_on timestamp with time zone NOT NULL,
    last_modified timestamp with time zone NOT NULL,
    number character varying(100) NOT NULL,
    subject character varying(100) NOT NULL,
    body character varying(100) NOT NULL,
    order_amount integer NOT NULL,
    contact_name character varying(100) NOT NULL,
    contact_email character varying(100) NOT NULL,
    contact_country character varying(100) NOT NULL,
    contact_phone character varying(100) NOT NULL,
    contact_website character varying(100) NOT NULL,
    customer_id integer NOT NULL,
    order_amount_unit_id integer NOT NULL,
    product_id integer NOT NULL
);


ALTER TABLE customer_order OWNER TO invest;

--
-- TOC entry 207 (class 1259 OID 207495)
-- Name: customer_order_id_seq; Type: SEQUENCE; Schema: public; Owner: invest
--

CREATE SEQUENCE customer_order_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE customer_order_id_seq OWNER TO invest;

--
-- TOC entry 2394 (class 0 OID 0)
-- Dependencies: 207
-- Name: customer_order_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: invest
--

ALTER SEQUENCE customer_order_id_seq OWNED BY customer_order.id;


--
-- TOC entry 210 (class 1259 OID 207510)
-- Name: customer_orderdiscuss; Type: TABLE; Schema: public; Owner: invest; Tablespace: 
--

CREATE TABLE customer_orderdiscuss (
    id integer NOT NULL,
    created_on timestamp with time zone NOT NULL,
    last_modified timestamp with time zone NOT NULL,
    order_id integer NOT NULL
);


ALTER TABLE customer_orderdiscuss OWNER TO invest;

--
-- TOC entry 209 (class 1259 OID 207508)
-- Name: customer_orderdiscuss_id_seq; Type: SEQUENCE; Schema: public; Owner: invest
--

CREATE SEQUENCE customer_orderdiscuss_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE customer_orderdiscuss_id_seq OWNER TO invest;

--
-- TOC entry 2395 (class 0 OID 0)
-- Dependencies: 209
-- Name: customer_orderdiscuss_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: invest
--

ALTER SEQUENCE customer_orderdiscuss_id_seq OWNED BY customer_orderdiscuss.id;


--
-- TOC entry 218 (class 1259 OID 207618)
-- Name: customer_orderdiscuss_messages; Type: TABLE; Schema: public; Owner: invest; Tablespace: 
--

CREATE TABLE customer_orderdiscuss_messages (
    id integer NOT NULL,
    orderdiscuss_id integer NOT NULL,
    orderdiscussmessage_id integer NOT NULL
);


ALTER TABLE customer_orderdiscuss_messages OWNER TO invest;

--
-- TOC entry 217 (class 1259 OID 207616)
-- Name: customer_orderdiscuss_messages_id_seq; Type: SEQUENCE; Schema: public; Owner: invest
--

CREATE SEQUENCE customer_orderdiscuss_messages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE customer_orderdiscuss_messages_id_seq OWNER TO invest;

--
-- TOC entry 2396 (class 0 OID 0)
-- Dependencies: 217
-- Name: customer_orderdiscuss_messages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: invest
--

ALTER SEQUENCE customer_orderdiscuss_messages_id_seq OWNED BY customer_orderdiscuss_messages.id;


--
-- TOC entry 220 (class 1259 OID 207626)
-- Name: customer_orderdiscuss_order_files; Type: TABLE; Schema: public; Owner: invest; Tablespace: 
--

CREATE TABLE customer_orderdiscuss_order_files (
    id integer NOT NULL,
    orderdiscuss_id integer NOT NULL,
    file_id integer NOT NULL
);


ALTER TABLE customer_orderdiscuss_order_files OWNER TO invest;

--
-- TOC entry 219 (class 1259 OID 207624)
-- Name: customer_orderdiscuss_order_files_id_seq; Type: SEQUENCE; Schema: public; Owner: invest
--

CREATE SEQUENCE customer_orderdiscuss_order_files_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE customer_orderdiscuss_order_files_id_seq OWNER TO invest;

--
-- TOC entry 2397 (class 0 OID 0)
-- Dependencies: 219
-- Name: customer_orderdiscuss_order_files_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: invest
--

ALTER SEQUENCE customer_orderdiscuss_order_files_id_seq OWNED BY customer_orderdiscuss_order_files.id;


--
-- TOC entry 222 (class 1259 OID 207634)
-- Name: customer_orderdiscuss_participants; Type: TABLE; Schema: public; Owner: invest; Tablespace: 
--

CREATE TABLE customer_orderdiscuss_participants (
    id integer NOT NULL,
    orderdiscuss_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE customer_orderdiscuss_participants OWNER TO invest;

--
-- TOC entry 221 (class 1259 OID 207632)
-- Name: customer_orderdiscuss_participants_id_seq; Type: SEQUENCE; Schema: public; Owner: invest
--

CREATE SEQUENCE customer_orderdiscuss_participants_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE customer_orderdiscuss_participants_id_seq OWNER TO invest;

--
-- TOC entry 2398 (class 0 OID 0)
-- Dependencies: 221
-- Name: customer_orderdiscuss_participants_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: invest
--

ALTER SEQUENCE customer_orderdiscuss_participants_id_seq OWNED BY customer_orderdiscuss_participants.id;


--
-- TOC entry 212 (class 1259 OID 207518)
-- Name: customer_orderdiscussmessage; Type: TABLE; Schema: public; Owner: invest; Tablespace: 
--

CREATE TABLE customer_orderdiscussmessage (
    id integer NOT NULL,
    created_on timestamp with time zone NOT NULL,
    last_modified timestamp with time zone NOT NULL,
    message character varying(200) NOT NULL,
    author_id integer NOT NULL
);


ALTER TABLE customer_orderdiscussmessage OWNER TO invest;

--
-- TOC entry 211 (class 1259 OID 207516)
-- Name: customer_orderdiscussmessage_id_seq; Type: SEQUENCE; Schema: public; Owner: invest
--

CREATE SEQUENCE customer_orderdiscussmessage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE customer_orderdiscussmessage_id_seq OWNER TO invest;

--
-- TOC entry 2399 (class 0 OID 0)
-- Dependencies: 211
-- Name: customer_orderdiscussmessage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: invest
--

ALTER SEQUENCE customer_orderdiscussmessage_id_seq OWNED BY customer_orderdiscussmessage.id;


--
-- TOC entry 214 (class 1259 OID 207526)
-- Name: customer_photo; Type: TABLE; Schema: public; Owner: invest; Tablespace: 
--

CREATE TABLE customer_photo (
    id integer NOT NULL,
    created_on timestamp with time zone NOT NULL,
    last_modified timestamp with time zone NOT NULL,
    name character varying(100) NOT NULL,
    file character varying(100) NOT NULL
);


ALTER TABLE customer_photo OWNER TO invest;

--
-- TOC entry 213 (class 1259 OID 207524)
-- Name: customer_photo_id_seq; Type: SEQUENCE; Schema: public; Owner: invest
--

CREATE SEQUENCE customer_photo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE customer_photo_id_seq OWNER TO invest;

--
-- TOC entry 2400 (class 0 OID 0)
-- Dependencies: 213
-- Name: customer_photo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: invest
--

ALTER SEQUENCE customer_photo_id_seq OWNED BY customer_photo.id;


--
-- TOC entry 200 (class 1259 OID 202456)
-- Name: customer_product; Type: TABLE; Schema: public; Owner: invest; Tablespace: 
--

CREATE TABLE customer_product (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    amount integer NOT NULL,
    power integer NOT NULL,
    min_order integer NOT NULL,
    category_id integer NOT NULL,
    company_id integer NOT NULL,
    created_on timestamp with time zone NOT NULL,
    last_modified timestamp with time zone NOT NULL
);


ALTER TABLE customer_product OWNER TO invest;

--
-- TOC entry 199 (class 1259 OID 202454)
-- Name: customer_product_id_seq; Type: SEQUENCE; Schema: public; Owner: invest
--

CREATE SEQUENCE customer_product_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE customer_product_id_seq OWNER TO invest;

--
-- TOC entry 2401 (class 0 OID 0)
-- Dependencies: 199
-- Name: customer_product_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: invest
--

ALTER SEQUENCE customer_product_id_seq OWNED BY customer_product.id;


--
-- TOC entry 202 (class 1259 OID 202470)
-- Name: customer_productcategory; Type: TABLE; Schema: public; Owner: invest; Tablespace: 
--

CREATE TABLE customer_productcategory (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    parent_id integer,
    created_on timestamp with time zone NOT NULL,
    last_modified timestamp with time zone NOT NULL
);


ALTER TABLE customer_productcategory OWNER TO invest;

--
-- TOC entry 201 (class 1259 OID 202468)
-- Name: customer_productcategory_id_seq; Type: SEQUENCE; Schema: public; Owner: invest
--

CREATE SEQUENCE customer_productcategory_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE customer_productcategory_id_seq OWNER TO invest;

--
-- TOC entry 2402 (class 0 OID 0)
-- Dependencies: 201
-- Name: customer_productcategory_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: invest
--

ALTER SEQUENCE customer_productcategory_id_seq OWNED BY customer_productcategory.id;


--
-- TOC entry 216 (class 1259 OID 207534)
-- Name: customer_unit; Type: TABLE; Schema: public; Owner: invest; Tablespace: 
--

CREATE TABLE customer_unit (
    id integer NOT NULL,
    created_on timestamp with time zone NOT NULL,
    last_modified timestamp with time zone NOT NULL,
    name character varying(100) NOT NULL
);


ALTER TABLE customer_unit OWNER TO invest;

--
-- TOC entry 215 (class 1259 OID 207532)
-- Name: customer_unit_id_seq; Type: SEQUENCE; Schema: public; Owner: invest
--

CREATE SEQUENCE customer_unit_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE customer_unit_id_seq OWNER TO invest;

--
-- TOC entry 2403 (class 0 OID 0)
-- Dependencies: 215
-- Name: customer_unit_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: invest
--

ALTER SEQUENCE customer_unit_id_seq OWNED BY customer_unit.id;


--
-- TOC entry 189 (class 1259 OID 182257)
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: invest; Tablespace: 
--

CREATE TABLE django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE django_admin_log OWNER TO invest;

--
-- TOC entry 188 (class 1259 OID 182255)
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: invest
--

CREATE SEQUENCE django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_admin_log_id_seq OWNER TO invest;

--
-- TOC entry 2404 (class 0 OID 0)
-- Dependencies: 188
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: invest
--

ALTER SEQUENCE django_admin_log_id_seq OWNED BY django_admin_log.id;


--
-- TOC entry 175 (class 1259 OID 182143)
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: invest; Tablespace: 
--

CREATE TABLE django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE django_content_type OWNER TO invest;

--
-- TOC entry 174 (class 1259 OID 182141)
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: invest
--

CREATE SEQUENCE django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_content_type_id_seq OWNER TO invest;

--
-- TOC entry 2405 (class 0 OID 0)
-- Dependencies: 174
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: invest
--

ALTER SEQUENCE django_content_type_id_seq OWNED BY django_content_type.id;


--
-- TOC entry 173 (class 1259 OID 182132)
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: invest; Tablespace: 
--

CREATE TABLE django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE django_migrations OWNER TO invest;

--
-- TOC entry 172 (class 1259 OID 182130)
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: invest
--

CREATE SEQUENCE django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_migrations_id_seq OWNER TO invest;

--
-- TOC entry 2406 (class 0 OID 0)
-- Dependencies: 172
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: invest
--

ALTER SEQUENCE django_migrations_id_seq OWNED BY django_migrations.id;


--
-- TOC entry 190 (class 1259 OID 182286)
-- Name: django_session; Type: TABLE; Schema: public; Owner: invest; Tablespace: 
--

CREATE TABLE django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE django_session OWNER TO invest;

--
-- TOC entry 192 (class 1259 OID 182299)
-- Name: polls_choice; Type: TABLE; Schema: public; Owner: invest; Tablespace: 
--

CREATE TABLE polls_choice (
    id integer NOT NULL,
    choice_text character varying(200) NOT NULL,
    votes integer NOT NULL,
    question_id integer NOT NULL
);


ALTER TABLE polls_choice OWNER TO invest;

--
-- TOC entry 191 (class 1259 OID 182297)
-- Name: polls_choice_id_seq; Type: SEQUENCE; Schema: public; Owner: invest
--

CREATE SEQUENCE polls_choice_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE polls_choice_id_seq OWNER TO invest;

--
-- TOC entry 2407 (class 0 OID 0)
-- Dependencies: 191
-- Name: polls_choice_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: invest
--

ALTER SEQUENCE polls_choice_id_seq OWNED BY polls_choice.id;


--
-- TOC entry 194 (class 1259 OID 182307)
-- Name: polls_question; Type: TABLE; Schema: public; Owner: invest; Tablespace: 
--

CREATE TABLE polls_question (
    id integer NOT NULL,
    question_text character varying(200) NOT NULL,
    pub_date timestamp with time zone NOT NULL
);


ALTER TABLE polls_question OWNER TO invest;

--
-- TOC entry 193 (class 1259 OID 182305)
-- Name: polls_question_id_seq; Type: SEQUENCE; Schema: public; Owner: invest
--

CREATE SEQUENCE polls_question_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE polls_question_id_seq OWNER TO invest;

--
-- TOC entry 2408 (class 0 OID 0)
-- Dependencies: 193
-- Name: polls_question_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: invest
--

ALTER SEQUENCE polls_question_id_seq OWNED BY polls_question.id;


--
-- TOC entry 2045 (class 2604 OID 182164)
-- Name: id; Type: DEFAULT; Schema: public; Owner: invest
--

ALTER TABLE ONLY auth_group ALTER COLUMN id SET DEFAULT nextval('auth_group_id_seq'::regclass);


--
-- TOC entry 2046 (class 2604 OID 182174)
-- Name: id; Type: DEFAULT; Schema: public; Owner: invest
--

ALTER TABLE ONLY auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('auth_group_permissions_id_seq'::regclass);


--
-- TOC entry 2044 (class 2604 OID 182156)
-- Name: id; Type: DEFAULT; Schema: public; Owner: invest
--

ALTER TABLE ONLY auth_permission ALTER COLUMN id SET DEFAULT nextval('auth_permission_id_seq'::regclass);


--
-- TOC entry 2047 (class 2604 OID 182182)
-- Name: id; Type: DEFAULT; Schema: public; Owner: invest
--

ALTER TABLE ONLY auth_user ALTER COLUMN id SET DEFAULT nextval('auth_user_id_seq'::regclass);


--
-- TOC entry 2048 (class 2604 OID 182192)
-- Name: id; Type: DEFAULT; Schema: public; Owner: invest
--

ALTER TABLE ONLY auth_user_groups ALTER COLUMN id SET DEFAULT nextval('auth_user_groups_id_seq'::regclass);


--
-- TOC entry 2049 (class 2604 OID 182200)
-- Name: id; Type: DEFAULT; Schema: public; Owner: invest
--

ALTER TABLE ONLY auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('auth_user_user_permissions_id_seq'::regclass);


--
-- TOC entry 2054 (class 2604 OID 202437)
-- Name: id; Type: DEFAULT; Schema: public; Owner: invest
--

ALTER TABLE ONLY customer_company ALTER COLUMN id SET DEFAULT nextval('customer_company_id_seq'::regclass);


--
-- TOC entry 2068 (class 2604 OID 207720)
-- Name: id; Type: DEFAULT; Schema: public; Owner: invest
--

ALTER TABLE ONLY customer_company_files ALTER COLUMN id SET DEFAULT nextval('customer_company_files_id_seq'::regclass);


--
-- TOC entry 2055 (class 2604 OID 202448)
-- Name: id; Type: DEFAULT; Schema: public; Owner: invest
--

ALTER TABLE ONLY customer_employee ALTER COLUMN id SET DEFAULT nextval('customer_employee_id_seq'::regclass);


--
-- TOC entry 2058 (class 2604 OID 207484)
-- Name: id; Type: DEFAULT; Schema: public; Owner: invest
--

ALTER TABLE ONLY customer_employeejobposition ALTER COLUMN id SET DEFAULT nextval('customer_employeejobposition_id_seq'::regclass);


--
-- TOC entry 2059 (class 2604 OID 207492)
-- Name: id; Type: DEFAULT; Schema: public; Owner: invest
--

ALTER TABLE ONLY customer_file ALTER COLUMN id SET DEFAULT nextval('customer_file_id_seq'::regclass);


--
-- TOC entry 2060 (class 2604 OID 207500)
-- Name: id; Type: DEFAULT; Schema: public; Owner: invest
--

ALTER TABLE ONLY customer_order ALTER COLUMN id SET DEFAULT nextval('customer_order_id_seq'::regclass);


--
-- TOC entry 2061 (class 2604 OID 207513)
-- Name: id; Type: DEFAULT; Schema: public; Owner: invest
--

ALTER TABLE ONLY customer_orderdiscuss ALTER COLUMN id SET DEFAULT nextval('customer_orderdiscuss_id_seq'::regclass);


--
-- TOC entry 2065 (class 2604 OID 207621)
-- Name: id; Type: DEFAULT; Schema: public; Owner: invest
--

ALTER TABLE ONLY customer_orderdiscuss_messages ALTER COLUMN id SET DEFAULT nextval('customer_orderdiscuss_messages_id_seq'::regclass);


--
-- TOC entry 2066 (class 2604 OID 207629)
-- Name: id; Type: DEFAULT; Schema: public; Owner: invest
--

ALTER TABLE ONLY customer_orderdiscuss_order_files ALTER COLUMN id SET DEFAULT nextval('customer_orderdiscuss_order_files_id_seq'::regclass);


--
-- TOC entry 2067 (class 2604 OID 207637)
-- Name: id; Type: DEFAULT; Schema: public; Owner: invest
--

ALTER TABLE ONLY customer_orderdiscuss_participants ALTER COLUMN id SET DEFAULT nextval('customer_orderdiscuss_participants_id_seq'::regclass);


--
-- TOC entry 2062 (class 2604 OID 207521)
-- Name: id; Type: DEFAULT; Schema: public; Owner: invest
--

ALTER TABLE ONLY customer_orderdiscussmessage ALTER COLUMN id SET DEFAULT nextval('customer_orderdiscussmessage_id_seq'::regclass);


--
-- TOC entry 2063 (class 2604 OID 207529)
-- Name: id; Type: DEFAULT; Schema: public; Owner: invest
--

ALTER TABLE ONLY customer_photo ALTER COLUMN id SET DEFAULT nextval('customer_photo_id_seq'::regclass);


--
-- TOC entry 2056 (class 2604 OID 202459)
-- Name: id; Type: DEFAULT; Schema: public; Owner: invest
--

ALTER TABLE ONLY customer_product ALTER COLUMN id SET DEFAULT nextval('customer_product_id_seq'::regclass);


--
-- TOC entry 2057 (class 2604 OID 202473)
-- Name: id; Type: DEFAULT; Schema: public; Owner: invest
--

ALTER TABLE ONLY customer_productcategory ALTER COLUMN id SET DEFAULT nextval('customer_productcategory_id_seq'::regclass);


--
-- TOC entry 2064 (class 2604 OID 207537)
-- Name: id; Type: DEFAULT; Schema: public; Owner: invest
--

ALTER TABLE ONLY customer_unit ALTER COLUMN id SET DEFAULT nextval('customer_unit_id_seq'::regclass);


--
-- TOC entry 2050 (class 2604 OID 182260)
-- Name: id; Type: DEFAULT; Schema: public; Owner: invest
--

ALTER TABLE ONLY django_admin_log ALTER COLUMN id SET DEFAULT nextval('django_admin_log_id_seq'::regclass);


--
-- TOC entry 2043 (class 2604 OID 182146)
-- Name: id; Type: DEFAULT; Schema: public; Owner: invest
--

ALTER TABLE ONLY django_content_type ALTER COLUMN id SET DEFAULT nextval('django_content_type_id_seq'::regclass);


--
-- TOC entry 2042 (class 2604 OID 182135)
-- Name: id; Type: DEFAULT; Schema: public; Owner: invest
--

ALTER TABLE ONLY django_migrations ALTER COLUMN id SET DEFAULT nextval('django_migrations_id_seq'::regclass);


--
-- TOC entry 2052 (class 2604 OID 182302)
-- Name: id; Type: DEFAULT; Schema: public; Owner: invest
--

ALTER TABLE ONLY polls_choice ALTER COLUMN id SET DEFAULT nextval('polls_choice_id_seq'::regclass);


--
-- TOC entry 2053 (class 2604 OID 182310)
-- Name: id; Type: DEFAULT; Schema: public; Owner: invest
--

ALTER TABLE ONLY polls_question ALTER COLUMN id SET DEFAULT nextval('polls_question_id_seq'::regclass);


--
-- TOC entry 2329 (class 0 OID 182161)
-- Dependencies: 179
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: invest
--

COPY auth_group (id, name) FROM stdin;
1	Sales
\.


--
-- TOC entry 2409 (class 0 OID 0)
-- Dependencies: 178
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: invest
--

SELECT pg_catalog.setval('auth_group_id_seq', 1, true);


--
-- TOC entry 2331 (class 0 OID 182171)
-- Dependencies: 181
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: invest
--

COPY auth_group_permissions (id, group_id, permission_id) FROM stdin;
1	1	13
2	1	14
3	1	15
4	1	19
5	1	20
6	1	21
7	1	22
8	1	23
9	1	24
\.


--
-- TOC entry 2410 (class 0 OID 0)
-- Dependencies: 180
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: invest
--

SELECT pg_catalog.setval('auth_group_permissions_id_seq', 9, true);


--
-- TOC entry 2327 (class 0 OID 182153)
-- Dependencies: 177
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: invest
--

COPY auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can add user	2	add_user
5	Can change user	2	change_user
6	Can delete user	2	delete_user
7	Can add group	3	add_group
8	Can change group	3	change_group
9	Can delete group	3	delete_group
10	Can add permission	4	add_permission
11	Can change permission	4	change_permission
12	Can delete permission	4	delete_permission
13	Can add content type	5	add_contenttype
14	Can change content type	5	change_contenttype
15	Can delete content type	5	delete_contenttype
16	Can add session	6	add_session
17	Can change session	6	change_session
18	Can delete session	6	delete_session
19	Can add choice	7	add_choice
20	Can change choice	7	change_choice
21	Can delete choice	7	delete_choice
22	Can add question	8	add_question
23	Can change question	8	change_question
24	Can delete question	8	delete_question
28	Can add employee	10	add_employee
29	Can change employee	10	change_employee
30	Can delete employee	10	delete_employee
31	Can add company	11	add_company
32	Can change company	11	change_company
33	Can delete company	11	delete_company
34	Can add product	12	add_product
35	Can change product	12	change_product
36	Can delete product	12	delete_product
37	Can add product category	13	add_productcategory
38	Can change product category	13	change_productcategory
39	Can delete product category	13	delete_productcategory
40	Can add photo	14	add_photo
41	Can change photo	14	change_photo
42	Can delete photo	14	delete_photo
43	Can add order discuss	15	add_orderdiscuss
44	Can change order discuss	15	change_orderdiscuss
45	Can delete order discuss	15	delete_orderdiscuss
46	Can add file	16	add_file
47	Can change file	16	change_file
48	Can delete file	16	delete_file
49	Can add order discuss message	17	add_orderdiscussmessage
50	Can change order discuss message	17	change_orderdiscussmessage
51	Can delete order discuss message	17	delete_orderdiscussmessage
52	Can add order	18	add_order
53	Can change order	18	change_order
54	Can delete order	18	delete_order
55	Can add employee job position	19	add_employeejobposition
56	Can change employee job position	19	change_employeejobposition
57	Can delete employee job position	19	delete_employeejobposition
58	Can add unit	20	add_unit
59	Can change unit	20	change_unit
60	Can delete unit	20	delete_unit
\.


--
-- TOC entry 2411 (class 0 OID 0)
-- Dependencies: 176
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: invest
--

SELECT pg_catalog.setval('auth_permission_id_seq', 60, true);


--
-- TOC entry 2333 (class 0 OID 182179)
-- Dependencies: 183
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: invest
--

COPY auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
2	pbkdf2_sha256$30000$COlH0TsWRlYL$5wKDaYvrdiLLL9IxAV6Xv3GUvVU3PouLWGAftxTgyck=	\N	f	user				f	t	2017-02-27 23:10:32+05
1	pbkdf2_sha256$30000$twjcKvh6yWu0$jWNDpqyIq55UNSo76GWasTHKqfZLOHG43w6FYCnldfw=	2017-04-01 11:50:28.807401+05	t	admin			admin@voicegetting.com	t	t	2017-02-27 23:08:27.649475+05
\.


--
-- TOC entry 2335 (class 0 OID 182189)
-- Dependencies: 185
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: invest
--

COPY auth_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- TOC entry 2412 (class 0 OID 0)
-- Dependencies: 184
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: invest
--

SELECT pg_catalog.setval('auth_user_groups_id_seq', 1, false);


--
-- TOC entry 2413 (class 0 OID 0)
-- Dependencies: 182
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: invest
--

SELECT pg_catalog.setval('auth_user_id_seq', 2, true);


--
-- TOC entry 2337 (class 0 OID 182197)
-- Dependencies: 187
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: invest
--

COPY auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- TOC entry 2414 (class 0 OID 0)
-- Dependencies: 186
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: invest
--

SELECT pg_catalog.setval('auth_user_user_permissions_id_seq', 1, false);


--
-- TOC entry 2346 (class 0 OID 202434)
-- Dependencies: 196
-- Data for Name: customer_company; Type: TABLE DATA; Schema: public; Owner: invest
--

COPY customer_company (id, name, address, phone, bank_name, bank_account_number, mfo, inn, okonx, website, email, company_type, foundation_date, employees_amount, production_power, notes, created_on, last_modified) FROM stdin;
2	nameA	AddressA	99854565616	BanknameA	21321321	123456	16565	12312356	http://www.website.uz	farguluz@mail.ru	SV	2007-03-09	3	4	notesa	2017-03-22 23:56:41.586927+05	2017-03-22 23:56:41.705933+05
1	nameA	AddressA	99854565616	BanknameA	21321321	123456	16565	12312356	http://www.website.uz	farguluz@mail.ru	SV	2007-03-09	3	4	notesa	2017-03-22 23:56:41.586927+05	2017-03-22 23:56:41.705933+05
3	nameA	AddressA	99854565616	BanknameA	21321321	123456	16565	12312356	http://www.website.uz	farguluz@mail.ru	SV	2007-03-09	3	4	notesa	2017-03-22 23:56:41.586927+05	2017-03-22 23:56:41.705933+05
4	NameD	addressD	302392309	banknameD	3230429340293	32039230	0932039230	92392323	http://asdsdf.com	asdf@asdf.com	SV	2007-03-09	3233	777	asdfasdf	2017-03-22 23:56:41.586927+05	2017-03-22 23:56:41.705933+05
\.


--
-- TOC entry 2374 (class 0 OID 207717)
-- Dependencies: 224
-- Data for Name: customer_company_files; Type: TABLE DATA; Schema: public; Owner: invest
--

COPY customer_company_files (id, company_id, file_id) FROM stdin;
\.


--
-- TOC entry 2415 (class 0 OID 0)
-- Dependencies: 223
-- Name: customer_company_files_id_seq; Type: SEQUENCE SET; Schema: public; Owner: invest
--

SELECT pg_catalog.setval('customer_company_files_id_seq', 1, false);


--
-- TOC entry 2416 (class 0 OID 0)
-- Dependencies: 195
-- Name: customer_company_id_seq; Type: SEQUENCE SET; Schema: public; Owner: invest
--

SELECT pg_catalog.setval('customer_company_id_seq', 4, true);


--
-- TOC entry 2348 (class 0 OID 202445)
-- Dependencies: 198
-- Data for Name: customer_employee; Type: TABLE DATA; Schema: public; Owner: invest
--

COPY customer_employee (id, first_name, last_name, middle_name, job_position_id, sex, certificates, email, phone, company_id, created_on, last_modified) FROM stdin;
1	Bob	Johns	M	\N	Male	CertificateA	email@emailb.uz	99854565616	1	2017-03-22 23:56:41.834941+05	2017-03-22 23:56:42.074954+05
2	Bob	Johns	M	\N	Male	CertificateA	email@emailb.uz	99854565616	3	2017-03-22 23:56:41.834941+05	2017-03-22 23:56:42.074954+05
3	Faradey	Johns	M	\N	Male	CertificateA	farguluz@mail.ru	987987987	2	2017-03-22 23:56:41.834941+05	2017-03-22 23:56:42.074954+05
\.


--
-- TOC entry 2417 (class 0 OID 0)
-- Dependencies: 197
-- Name: customer_employee_id_seq; Type: SEQUENCE SET; Schema: public; Owner: invest
--

SELECT pg_catalog.setval('customer_employee_id_seq', 3, true);


--
-- TOC entry 2354 (class 0 OID 207481)
-- Dependencies: 204
-- Data for Name: customer_employeejobposition; Type: TABLE DATA; Schema: public; Owner: invest
--

COPY customer_employeejobposition (id, created_on, last_modified, name) FROM stdin;
\.


--
-- TOC entry 2418 (class 0 OID 0)
-- Dependencies: 203
-- Name: customer_employeejobposition_id_seq; Type: SEQUENCE SET; Schema: public; Owner: invest
--

SELECT pg_catalog.setval('customer_employeejobposition_id_seq', 1, false);


--
-- TOC entry 2356 (class 0 OID 207489)
-- Dependencies: 206
-- Data for Name: customer_file; Type: TABLE DATA; Schema: public; Owner: invest
--

COPY customer_file (id, created_on, last_modified, name, file) FROM stdin;
1	2017-03-23 23:28:09.209869+05	2017-03-23 23:28:09.209869+05	bepositive.png	uploads/files/bepositive.png
2	2017-03-23 23:28:26.468856+05	2017-03-23 23:28:26.468856+05	innerstate.png	uploads/files/innerstate.png
3	2017-03-23 23:31:52.544643+05	2017-03-23 23:31:52.544643+05	drawing.svg	uploads/files/drawing.svg
4	2017-03-24 00:03:00.908507+05	2017-03-24 00:03:00.908507+05	DemoLogo-Saxar.jpg	uploads/files/DemoLogo-Saxar.jpg
5	2017-03-30 12:12:28.251213+05	2017-03-30 12:12:28.251213+05	Train_in_bangladesh.jpg	uploads/files/Train_in_bangladesh.jpg
\.


--
-- TOC entry 2419 (class 0 OID 0)
-- Dependencies: 205
-- Name: customer_file_id_seq; Type: SEQUENCE SET; Schema: public; Owner: invest
--

SELECT pg_catalog.setval('customer_file_id_seq', 5, true);


--
-- TOC entry 2358 (class 0 OID 207497)
-- Dependencies: 208
-- Data for Name: customer_order; Type: TABLE DATA; Schema: public; Owner: invest
--

COPY customer_order (id, created_on, last_modified, number, subject, body, order_amount, contact_name, contact_email, contact_country, contact_phone, contact_website, customer_id, order_amount_unit_id, product_id) FROM stdin;
1	2017-03-23 00:50:30.924634+05	2017-03-23 03:02:56.107073+05	100	SUBJECT	body	100	tom hanks	tom.hanks@email.com	Uzbekistan	929299292	http://wer.com	4	5	2
\.


--
-- TOC entry 2420 (class 0 OID 0)
-- Dependencies: 207
-- Name: customer_order_id_seq; Type: SEQUENCE SET; Schema: public; Owner: invest
--

SELECT pg_catalog.setval('customer_order_id_seq', 1, true);


--
-- TOC entry 2360 (class 0 OID 207510)
-- Dependencies: 210
-- Data for Name: customer_orderdiscuss; Type: TABLE DATA; Schema: public; Owner: invest
--

COPY customer_orderdiscuss (id, created_on, last_modified, order_id) FROM stdin;
1	2017-03-23 02:57:44.771+05	2017-03-30 12:12:28.439224+05	1
\.


--
-- TOC entry 2421 (class 0 OID 0)
-- Dependencies: 209
-- Name: customer_orderdiscuss_id_seq; Type: SEQUENCE SET; Schema: public; Owner: invest
--

SELECT pg_catalog.setval('customer_orderdiscuss_id_seq', 1, true);


--
-- TOC entry 2368 (class 0 OID 207618)
-- Dependencies: 218
-- Data for Name: customer_orderdiscuss_messages; Type: TABLE DATA; Schema: public; Owner: invest
--

COPY customer_orderdiscuss_messages (id, orderdiscuss_id, orderdiscussmessage_id) FROM stdin;
1	1	2
2	1	3
3	1	7
4	1	8
5	1	9
6	1	10
7	1	11
8	1	12
9	1	13
10	1	14
\.


--
-- TOC entry 2422 (class 0 OID 0)
-- Dependencies: 217
-- Name: customer_orderdiscuss_messages_id_seq; Type: SEQUENCE SET; Schema: public; Owner: invest
--

SELECT pg_catalog.setval('customer_orderdiscuss_messages_id_seq', 10, true);


--
-- TOC entry 2370 (class 0 OID 207626)
-- Dependencies: 220
-- Data for Name: customer_orderdiscuss_order_files; Type: TABLE DATA; Schema: public; Owner: invest
--

COPY customer_orderdiscuss_order_files (id, orderdiscuss_id, file_id) FROM stdin;
1	1	1
2	1	2
3	1	3
4	1	4
5	1	5
\.


--
-- TOC entry 2423 (class 0 OID 0)
-- Dependencies: 219
-- Name: customer_orderdiscuss_order_files_id_seq; Type: SEQUENCE SET; Schema: public; Owner: invest
--

SELECT pg_catalog.setval('customer_orderdiscuss_order_files_id_seq', 5, true);


--
-- TOC entry 2372 (class 0 OID 207634)
-- Dependencies: 222
-- Data for Name: customer_orderdiscuss_participants; Type: TABLE DATA; Schema: public; Owner: invest
--

COPY customer_orderdiscuss_participants (id, orderdiscuss_id, user_id) FROM stdin;
\.


--
-- TOC entry 2424 (class 0 OID 0)
-- Dependencies: 221
-- Name: customer_orderdiscuss_participants_id_seq; Type: SEQUENCE SET; Schema: public; Owner: invest
--

SELECT pg_catalog.setval('customer_orderdiscuss_participants_id_seq', 1, false);


--
-- TOC entry 2362 (class 0 OID 207518)
-- Dependencies: 212
-- Data for Name: customer_orderdiscussmessage; Type: TABLE DATA; Schema: public; Owner: invest
--

COPY customer_orderdiscussmessage (id, created_on, last_modified, message, author_id) FROM stdin;
2	2017-03-23 02:58:29.128+05	2017-03-23 02:58:29.128+05	hello world!!!	2
3	2017-03-23 02:58:50.007+05	2017-03-23 02:58:50.007+05	salom dunyo!!!	1
4	2017-03-23 03:03:35.75534+05	2017-03-23 03:03:35.75534+05	salom	2
7	2017-03-23 19:59:43.857604+05	2017-03-23 19:59:43.857604+05	salom salom	1
8	2017-03-23 20:00:16.972498+05	2017-03-23 20:00:16.973498+05	qandaysan&	1
9	2017-03-23 20:00:26.835062+05	2017-03-23 20:00:26.836062+05	yaxshimisan?	1
10	2017-03-23 20:00:33.199426+05	2017-03-23 20:00:33.199426+05	нима гаплар?	1
11	2017-03-23 20:00:40.503844+05	2017-03-23 20:00:40.504844+05	қанақа янгиликлар?	1
12	2017-03-23 23:04:03.890202+05	2017-03-23 23:04:03.891202+05	қанақа янгиликлар?	1
13	2017-03-24 00:01:18.986678+05	2017-03-24 00:01:18.986678+05	yangilik yo'qmi	1
14	2017-03-24 00:01:27.368157+05	2017-03-24 00:01:27.368157+05	namuncha	1
\.


--
-- TOC entry 2425 (class 0 OID 0)
-- Dependencies: 211
-- Name: customer_orderdiscussmessage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: invest
--

SELECT pg_catalog.setval('customer_orderdiscussmessage_id_seq', 14, true);


--
-- TOC entry 2364 (class 0 OID 207526)
-- Dependencies: 214
-- Data for Name: customer_photo; Type: TABLE DATA; Schema: public; Owner: invest
--

COPY customer_photo (id, created_on, last_modified, name, file) FROM stdin;
\.


--
-- TOC entry 2426 (class 0 OID 0)
-- Dependencies: 213
-- Name: customer_photo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: invest
--

SELECT pg_catalog.setval('customer_photo_id_seq', 1, false);


--
-- TOC entry 2350 (class 0 OID 202456)
-- Dependencies: 200
-- Data for Name: customer_product; Type: TABLE DATA; Schema: public; Owner: invest
--

COPY customer_product (id, name, amount, power, min_order, category_id, company_id, created_on, last_modified) FROM stdin;
1	Futbolka Paxta modelA	10	10000	100	2	2	2017-03-22 23:56:42.287967+05	2017-03-22 23:56:42.500979+05
2	Megamix Shpaklyovka ef10	1000	100000	100	6	4	2017-03-22 23:56:42.287967+05	2017-03-22 23:56:42.500979+05
\.


--
-- TOC entry 2427 (class 0 OID 0)
-- Dependencies: 199
-- Name: customer_product_id_seq; Type: SEQUENCE SET; Schema: public; Owner: invest
--

SELECT pg_catalog.setval('customer_product_id_seq', 2, true);


--
-- TOC entry 2352 (class 0 OID 202470)
-- Dependencies: 202
-- Data for Name: customer_productcategory; Type: TABLE DATA; Schema: public; Owner: invest
--

COPY customer_productcategory (id, name, parent_id, created_on, last_modified) FROM stdin;
1	Kiyim kechak	\N	2017-03-22 23:56:42.652988+05	2017-03-22 23:56:42.787995+05
2	Futbolka	1	2017-03-22 23:56:42.652988+05	2017-03-22 23:56:42.787995+05
3	Ko'ylak	1	2017-03-22 23:56:42.652988+05	2017-03-22 23:56:42.787995+05
4	Qurilish mollari	\N	2017-03-22 23:56:42.652988+05	2017-03-22 23:56:42.787995+05
5	Shpaklyovka	4	2017-03-22 23:56:42.652988+05	2017-03-22 23:56:42.787995+05
6	Megamix shpaklyovka	5	2017-03-22 23:56:42.652988+05	2017-03-22 23:56:42.787995+05
\.


--
-- TOC entry 2428 (class 0 OID 0)
-- Dependencies: 201
-- Name: customer_productcategory_id_seq; Type: SEQUENCE SET; Schema: public; Owner: invest
--

SELECT pg_catalog.setval('customer_productcategory_id_seq', 6, true);


--
-- TOC entry 2366 (class 0 OID 207534)
-- Dependencies: 216
-- Data for Name: customer_unit; Type: TABLE DATA; Schema: public; Owner: invest
--

COPY customer_unit (id, created_on, last_modified, name) FROM stdin;
3	2017-03-22 23:58:20.494+05	2017-03-22 23:58:20.494+05	Kg
5	2017-03-22 23:58:33.736+05	2017-03-22 23:58:33.736+05	Ton
\.


--
-- TOC entry 2429 (class 0 OID 0)
-- Dependencies: 215
-- Name: customer_unit_id_seq; Type: SEQUENCE SET; Schema: public; Owner: invest
--

SELECT pg_catalog.setval('customer_unit_id_seq', 5, true);


--
-- TOC entry 2339 (class 0 OID 182257)
-- Dependencies: 189
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: invest
--

COPY django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2017-02-27 23:10:33.018646+05	2	user	1	[{"added": {}}]	2	1
2	2017-02-27 23:11:34.46416+05	2	user	2	[]	2	1
3	2017-02-27 23:12:24.018995+05	1	Sales	1	[{"added": {}}]	3	1
4	2017-02-28 17:54:54.028445+05	6	Are you hungry?	2	[]	8	1
5	2017-02-28 17:55:11.446441+05	1	What's new?	2	[{"changed": {"fields": ["question_text"]}}]	8	1
6	2017-02-28 17:55:34.671769+05	7	Your favorite food?	1	[{"added": {}}]	8	1
7	2017-02-28 17:57:02.853813+05	4	Palov	1	[{"added": {}}]	7	1
8	2017-02-28 17:57:15.268523+05	5	Moshho'rda	1	[{"added": {}}]	7	1
9	2017-02-28 17:57:24.31404+05	6	Somsa	1	[{"added": {}}]	7	1
10	2017-02-28 17:57:31.401446+05	7	Manti	1	[{"added": {}}]	7	1
11	2017-02-28 17:57:39.379902+05	8	Sho'rva	1	[{"added": {}}]	7	1
12	2017-02-28 17:57:54.394761+05	7	Yaxshi ko'rgan ovqatingiz?	2	[{"changed": {"fields": ["question_text"]}}]	8	1
13	2017-02-28 17:58:03.204265+05	9	Tuppa	1	[{"added": {}}]	7	1
14	2017-02-28 17:58:13.527855+05	10	Kabob	1	[{"added": {}}]	7	1
15	2017-02-28 17:58:20.621261+05	11	Qazon kabob	1	[{"added": {}}]	7	1
16	2017-02-28 17:58:34.109033+05	4	Palov	2	[{"changed": {"fields": ["votes"]}}]	7	1
17	2017-02-28 17:58:48.193838+05	11	Qozon kabob	2	[{"changed": {"fields": ["choice_text"]}}]	7	1
18	2017-02-28 17:58:59.876506+05	5	Moshho'rda	2	[{"changed": {"fields": ["votes"]}}]	7	1
19	2017-03-09 13:29:13.325083+05	1	nameA	1	[{"added": {}}]	\N	1
20	2017-03-09 13:30:17.869775+05	2	nameB	1	[{"added": {}}]	\N	1
21	2017-03-23 03:02:56.114073+05	1	100	2	[]	18	1
22	2017-03-23 03:03:35.765341+05	4	OrderDiscussMessage object	1	[{"added": {}}]	17	1
\.


--
-- TOC entry 2430 (class 0 OID 0)
-- Dependencies: 188
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: invest
--

SELECT pg_catalog.setval('django_admin_log_id_seq', 22, true);


--
-- TOC entry 2325 (class 0 OID 182143)
-- Dependencies: 175
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: invest
--

COPY django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	user
3	auth	group
4	auth	permission
5	contenttypes	contenttype
6	sessions	session
7	polls	choice
8	polls	question
10	customer	employee
11	customer	company
12	customer	product
13	customer	productcategory
14	customer	photo
15	customer	orderdiscuss
16	customer	file
17	customer	orderdiscussmessage
18	customer	order
19	customer	employeejobposition
20	customer	unit
\.


--
-- TOC entry 2431 (class 0 OID 0)
-- Dependencies: 174
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: invest
--

SELECT pg_catalog.setval('django_content_type_id_seq', 20, true);


--
-- TOC entry 2323 (class 0 OID 182132)
-- Dependencies: 173
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: invest
--

COPY django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2017-02-27 18:45:55.074511+05
2	auth	0001_initial	2017-02-27 18:45:55.948561+05
3	admin	0001_initial	2017-02-27 18:45:56.28058+05
4	admin	0002_logentry_remove_auto_add	2017-02-27 18:45:56.319582+05
5	contenttypes	0002_remove_content_type_name	2017-02-27 18:45:56.391587+05
6	auth	0002_alter_permission_name_max_length	2017-02-27 18:45:56.480592+05
7	auth	0003_alter_user_email_max_length	2017-02-27 18:45:56.513594+05
8	auth	0004_alter_user_username_opts	2017-02-27 18:45:56.539595+05
9	auth	0005_alter_user_last_login_null	2017-02-27 18:45:56.569597+05
10	auth	0006_require_contenttypes_0002	2017-02-27 18:45:56.576597+05
11	auth	0007_alter_validators_add_error_messages	2017-02-27 18:45:56.608599+05
12	auth	0008_alter_user_username_max_length	2017-02-27 18:45:56.739606+05
13	sessions	0001_initial	2017-02-27 18:45:56.944618+05
14	polls	0001_initial	2017-02-27 22:48:05.520573+05
15	customer	0001_initial	2017-03-09 13:24:28.054767+05
16	customer	0002_auto_20170312_2102	2017-03-12 21:02:11.590521+05
17	customer	0003_auto_20170316_1557	2017-03-16 15:57:52.432844+05
18	customer	0004_product_company	2017-03-16 16:04:52.973897+05
19	customer	0005_auto_20170322_2349	2017-03-22 23:56:44.79111+05
20	customer	0006_auto_20170323_0246	2017-03-23 02:46:21.859205+05
\.


--
-- TOC entry 2432 (class 0 OID 0)
-- Dependencies: 172
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: invest
--

SELECT pg_catalog.setval('django_migrations_id_seq', 20, true);


--
-- TOC entry 2340 (class 0 OID 182286)
-- Dependencies: 190
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: invest
--

COPY django_session (session_key, session_data, expire_date) FROM stdin;
m76swocn3eii2rnhchtcxbokmcgq61hl	NjA4ZGY5YmNhYjY3YzRhNDc2MWZhNGQwNWNiNDJkZThmYzg1NTNiOTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI3OWExYjMxNTcxM2IxYWY2NGIyMTk3ZDUzZWY3ODY1ZTEyNzY4ZmIwIn0=	2017-03-13 23:09:31.958153+05
2waupymmqw45lxkvzkcv55ls0wlbtrpw	NGI5MzYzNDhmNDk3MDU0MGMyZDc1YjhmYzliNmZmNTNkZTI1ZmNjYzp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJiOGU0NDc1YjUyMGMxMTMyMmY4OGM2NzQ3NDRiMGYwOGJhNTAzNDYzIn0=	2017-03-21 20:23:43.62013+05
m4f2ndtuvd9m5vk7tff5hwx4pv05gif4	NGI5MzYzNDhmNDk3MDU0MGMyZDc1YjhmYzliNmZmNTNkZTI1ZmNjYzp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJiOGU0NDc1YjUyMGMxMTMyMmY4OGM2NzQ3NDRiMGYwOGJhNTAzNDYzIn0=	2017-04-06 02:53:26.005465+05
i46hj51gxs2bk4874ibeyrney2patbgx	NGI5MzYzNDhmNDk3MDU0MGMyZDc1YjhmYzliNmZmNTNkZTI1ZmNjYzp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJiOGU0NDc1YjUyMGMxMTMyMmY4OGM2NzQ3NDRiMGYwOGJhNTAzNDYzIn0=	2017-04-15 11:50:28.815401+05
\.


--
-- TOC entry 2342 (class 0 OID 182299)
-- Dependencies: 192
-- Data for Name: polls_choice; Type: TABLE DATA; Schema: public; Owner: invest
--

COPY polls_choice (id, choice_text, votes, question_id) FROM stdin;
2	The sky	0	1
6	Somsa	0	7
7	Manti	0	7
8	Sho'rva	0	7
9	Tuppa	0	7
5	Moshho'rda	0	7
11	Qozon kabob	1	7
4	Palov	1	7
10	Kabob	1	7
1	Not much	1	1
\.


--
-- TOC entry 2433 (class 0 OID 0)
-- Dependencies: 191
-- Name: polls_choice_id_seq; Type: SEQUENCE SET; Schema: public; Owner: invest
--

SELECT pg_catalog.setval('polls_choice_id_seq', 11, true);


--
-- TOC entry 2344 (class 0 OID 182307)
-- Dependencies: 194
-- Data for Name: polls_question; Type: TABLE DATA; Schema: public; Owner: invest
--

COPY polls_question (id, question_text, pub_date) FROM stdin;
6	Are you hungry?	2017-02-28 14:19:08+05
1	What's new?	2017-02-27 22:51:38+05
7	Yaxshi ko'rgan ovqatingiz?	2017-02-28 17:55:32+05
4	How are you?	2018-02-28 14:18:43.581+05
\.


--
-- TOC entry 2434 (class 0 OID 0)
-- Dependencies: 193
-- Name: polls_question_id_seq; Type: SEQUENCE SET; Schema: public; Owner: invest
--

SELECT pg_catalog.setval('polls_question_id_seq', 7, true);


--
-- TOC entry 2082 (class 2606 OID 182168)
-- Name: auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: invest; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- TOC entry 2088 (class 2606 OID 182223)
-- Name: auth_group_permissions_group_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: invest; Tablespace: 
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- TOC entry 2090 (class 2606 OID 182176)
-- Name: auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: invest; Tablespace: 
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- TOC entry 2084 (class 2606 OID 182166)
-- Name: auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: invest; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- TOC entry 2077 (class 2606 OID 182209)
-- Name: auth_permission_content_type_id_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: invest; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- TOC entry 2079 (class 2606 OID 182158)
-- Name: auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: invest; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- TOC entry 2099 (class 2606 OID 182194)
-- Name: auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: invest; Tablespace: 
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- TOC entry 2101 (class 2606 OID 182238)
-- Name: auth_user_groups_user_id_94350c0c_uniq; Type: CONSTRAINT; Schema: public; Owner: invest; Tablespace: 
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- TOC entry 2092 (class 2606 OID 182184)
-- Name: auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: invest; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- TOC entry 2105 (class 2606 OID 182202)
-- Name: auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: invest; Tablespace: 
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- TOC entry 2107 (class 2606 OID 182252)
-- Name: auth_user_user_permissions_user_id_14a6b632_uniq; Type: CONSTRAINT; Schema: public; Owner: invest; Tablespace: 
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- TOC entry 2095 (class 2606 OID 182281)
-- Name: auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: invest; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- TOC entry 2182 (class 2606 OID 207741)
-- Name: customer_company_files_company_id_e4f901bd_uniq; Type: CONSTRAINT; Schema: public; Owner: invest; Tablespace: 
--

ALTER TABLE ONLY customer_company_files
    ADD CONSTRAINT customer_company_files_company_id_e4f901bd_uniq UNIQUE (company_id, file_id);


--
-- TOC entry 2184 (class 2606 OID 207722)
-- Name: customer_company_files_pkey; Type: CONSTRAINT; Schema: public; Owner: invest; Tablespace: 
--

ALTER TABLE ONLY customer_company_files
    ADD CONSTRAINT customer_company_files_pkey PRIMARY KEY (id);


--
-- TOC entry 2122 (class 2606 OID 202442)
-- Name: customer_company_pkey; Type: CONSTRAINT; Schema: public; Owner: invest; Tablespace: 
--

ALTER TABLE ONLY customer_company
    ADD CONSTRAINT customer_company_pkey PRIMARY KEY (id);


--
-- TOC entry 2126 (class 2606 OID 202453)
-- Name: customer_employee_pkey; Type: CONSTRAINT; Schema: public; Owner: invest; Tablespace: 
--

ALTER TABLE ONLY customer_employee
    ADD CONSTRAINT customer_employee_pkey PRIMARY KEY (id);


--
-- TOC entry 2135 (class 2606 OID 207486)
-- Name: customer_employeejobposition_pkey; Type: CONSTRAINT; Schema: public; Owner: invest; Tablespace: 
--

ALTER TABLE ONLY customer_employeejobposition
    ADD CONSTRAINT customer_employeejobposition_pkey PRIMARY KEY (id);


--
-- TOC entry 2137 (class 2606 OID 207494)
-- Name: customer_file_pkey; Type: CONSTRAINT; Schema: public; Owner: invest; Tablespace: 
--

ALTER TABLE ONLY customer_file
    ADD CONSTRAINT customer_file_pkey PRIMARY KEY (id);


--
-- TOC entry 2143 (class 2606 OID 207507)
-- Name: customer_order_number_key; Type: CONSTRAINT; Schema: public; Owner: invest; Tablespace: 
--

ALTER TABLE ONLY customer_order
    ADD CONSTRAINT customer_order_number_key UNIQUE (number);


--
-- TOC entry 2145 (class 2606 OID 207505)
-- Name: customer_order_pkey; Type: CONSTRAINT; Schema: public; Owner: invest; Tablespace: 
--

ALTER TABLE ONLY customer_order
    ADD CONSTRAINT customer_order_pkey PRIMARY KEY (id);


--
-- TOC entry 2164 (class 2606 OID 207659)
-- Name: customer_orderdiscuss_messages_orderdiscuss_id_3bca1a3f_uniq; Type: CONSTRAINT; Schema: public; Owner: invest; Tablespace: 
--

ALTER TABLE ONLY customer_orderdiscuss_messages
    ADD CONSTRAINT customer_orderdiscuss_messages_orderdiscuss_id_3bca1a3f_uniq UNIQUE (orderdiscuss_id, orderdiscussmessage_id);


--
-- TOC entry 2166 (class 2606 OID 207623)
-- Name: customer_orderdiscuss_messages_pkey; Type: CONSTRAINT; Schema: public; Owner: invest; Tablespace: 
--

ALTER TABLE ONLY customer_orderdiscuss_messages
    ADD CONSTRAINT customer_orderdiscuss_messages_pkey PRIMARY KEY (id);


--
-- TOC entry 2170 (class 2606 OID 207679)
-- Name: customer_orderdiscuss_order_files_orderdiscuss_id_d52e3b82_uniq; Type: CONSTRAINT; Schema: public; Owner: invest; Tablespace: 
--

ALTER TABLE ONLY customer_orderdiscuss_order_files
    ADD CONSTRAINT customer_orderdiscuss_order_files_orderdiscuss_id_d52e3b82_uniq UNIQUE (orderdiscuss_id, file_id);


--
-- TOC entry 2172 (class 2606 OID 207631)
-- Name: customer_orderdiscuss_order_files_pkey; Type: CONSTRAINT; Schema: public; Owner: invest; Tablespace: 
--

ALTER TABLE ONLY customer_orderdiscuss_order_files
    ADD CONSTRAINT customer_orderdiscuss_order_files_pkey PRIMARY KEY (id);


--
-- TOC entry 2148 (class 2606 OID 207724)
-- Name: customer_orderdiscuss_order_id_6d139333_uniq; Type: CONSTRAINT; Schema: public; Owner: invest; Tablespace: 
--

ALTER TABLE ONLY customer_orderdiscuss
    ADD CONSTRAINT customer_orderdiscuss_order_id_6d139333_uniq UNIQUE (order_id);


--
-- TOC entry 2174 (class 2606 OID 207693)
-- Name: customer_orderdiscuss_participant_orderdiscuss_id_84cf5035_uniq; Type: CONSTRAINT; Schema: public; Owner: invest; Tablespace: 
--

ALTER TABLE ONLY customer_orderdiscuss_participants
    ADD CONSTRAINT customer_orderdiscuss_participant_orderdiscuss_id_84cf5035_uniq UNIQUE (orderdiscuss_id, user_id);


--
-- TOC entry 2178 (class 2606 OID 207639)
-- Name: customer_orderdiscuss_participants_pkey; Type: CONSTRAINT; Schema: public; Owner: invest; Tablespace: 
--

ALTER TABLE ONLY customer_orderdiscuss_participants
    ADD CONSTRAINT customer_orderdiscuss_participants_pkey PRIMARY KEY (id);


--
-- TOC entry 2150 (class 2606 OID 207515)
-- Name: customer_orderdiscuss_pkey; Type: CONSTRAINT; Schema: public; Owner: invest; Tablespace: 
--

ALTER TABLE ONLY customer_orderdiscuss
    ADD CONSTRAINT customer_orderdiscuss_pkey PRIMARY KEY (id);


--
-- TOC entry 2153 (class 2606 OID 207523)
-- Name: customer_orderdiscussmessage_pkey; Type: CONSTRAINT; Schema: public; Owner: invest; Tablespace: 
--

ALTER TABLE ONLY customer_orderdiscussmessage
    ADD CONSTRAINT customer_orderdiscussmessage_pkey PRIMARY KEY (id);


--
-- TOC entry 2155 (class 2606 OID 207531)
-- Name: customer_photo_pkey; Type: CONSTRAINT; Schema: public; Owner: invest; Tablespace: 
--

ALTER TABLE ONLY customer_photo
    ADD CONSTRAINT customer_photo_pkey PRIMARY KEY (id);


--
-- TOC entry 2130 (class 2606 OID 202461)
-- Name: customer_product_pkey; Type: CONSTRAINT; Schema: public; Owner: invest; Tablespace: 
--

ALTER TABLE ONLY customer_product
    ADD CONSTRAINT customer_product_pkey PRIMARY KEY (id);


--
-- TOC entry 2133 (class 2606 OID 202475)
-- Name: customer_productcategory_pkey; Type: CONSTRAINT; Schema: public; Owner: invest; Tablespace: 
--

ALTER TABLE ONLY customer_productcategory
    ADD CONSTRAINT customer_productcategory_pkey PRIMARY KEY (id);


--
-- TOC entry 2158 (class 2606 OID 207541)
-- Name: customer_unit_name_key; Type: CONSTRAINT; Schema: public; Owner: invest; Tablespace: 
--

ALTER TABLE ONLY customer_unit
    ADD CONSTRAINT customer_unit_name_key UNIQUE (name);


--
-- TOC entry 2160 (class 2606 OID 207539)
-- Name: customer_unit_pkey; Type: CONSTRAINT; Schema: public; Owner: invest; Tablespace: 
--

ALTER TABLE ONLY customer_unit
    ADD CONSTRAINT customer_unit_pkey PRIMARY KEY (id);


--
-- TOC entry 2111 (class 2606 OID 182266)
-- Name: django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: invest; Tablespace: 
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- TOC entry 2072 (class 2606 OID 182150)
-- Name: django_content_type_app_label_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: invest; Tablespace: 
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_app_label_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- TOC entry 2074 (class 2606 OID 182148)
-- Name: django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: invest; Tablespace: 
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- TOC entry 2070 (class 2606 OID 182140)
-- Name: django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: invest; Tablespace: 
--

ALTER TABLE ONLY django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- TOC entry 2114 (class 2606 OID 182293)
-- Name: django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: invest; Tablespace: 
--

ALTER TABLE ONLY django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- TOC entry 2118 (class 2606 OID 182304)
-- Name: polls_choice_pkey; Type: CONSTRAINT; Schema: public; Owner: invest; Tablespace: 
--

ALTER TABLE ONLY polls_choice
    ADD CONSTRAINT polls_choice_pkey PRIMARY KEY (id);


--
-- TOC entry 2120 (class 2606 OID 182312)
-- Name: polls_question_pkey; Type: CONSTRAINT; Schema: public; Owner: invest; Tablespace: 
--

ALTER TABLE ONLY polls_question
    ADD CONSTRAINT polls_question_pkey PRIMARY KEY (id);


--
-- TOC entry 2080 (class 1259 OID 182211)
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: invest; Tablespace: 
--

CREATE INDEX auth_group_name_a6ea08ec_like ON auth_group USING btree (name varchar_pattern_ops);


--
-- TOC entry 2085 (class 1259 OID 182224)
-- Name: auth_group_permissions_0e939a4f; Type: INDEX; Schema: public; Owner: invest; Tablespace: 
--

CREATE INDEX auth_group_permissions_0e939a4f ON auth_group_permissions USING btree (group_id);


--
-- TOC entry 2086 (class 1259 OID 182225)
-- Name: auth_group_permissions_8373b171; Type: INDEX; Schema: public; Owner: invest; Tablespace: 
--

CREATE INDEX auth_group_permissions_8373b171 ON auth_group_permissions USING btree (permission_id);


--
-- TOC entry 2075 (class 1259 OID 182210)
-- Name: auth_permission_417f1b1c; Type: INDEX; Schema: public; Owner: invest; Tablespace: 
--

CREATE INDEX auth_permission_417f1b1c ON auth_permission USING btree (content_type_id);


--
-- TOC entry 2096 (class 1259 OID 182240)
-- Name: auth_user_groups_0e939a4f; Type: INDEX; Schema: public; Owner: invest; Tablespace: 
--

CREATE INDEX auth_user_groups_0e939a4f ON auth_user_groups USING btree (group_id);


--
-- TOC entry 2097 (class 1259 OID 182239)
-- Name: auth_user_groups_e8701ad4; Type: INDEX; Schema: public; Owner: invest; Tablespace: 
--

CREATE INDEX auth_user_groups_e8701ad4 ON auth_user_groups USING btree (user_id);


--
-- TOC entry 2102 (class 1259 OID 182254)
-- Name: auth_user_user_permissions_8373b171; Type: INDEX; Schema: public; Owner: invest; Tablespace: 
--

CREATE INDEX auth_user_user_permissions_8373b171 ON auth_user_user_permissions USING btree (permission_id);


--
-- TOC entry 2103 (class 1259 OID 182253)
-- Name: auth_user_user_permissions_e8701ad4; Type: INDEX; Schema: public; Owner: invest; Tablespace: 
--

CREATE INDEX auth_user_user_permissions_e8701ad4 ON auth_user_user_permissions USING btree (user_id);


--
-- TOC entry 2093 (class 1259 OID 182282)
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: public; Owner: invest; Tablespace: 
--

CREATE INDEX auth_user_username_6821ab7c_like ON auth_user USING btree (username varchar_pattern_ops);


--
-- TOC entry 2179 (class 1259 OID 207742)
-- Name: customer_company_files_447d3092; Type: INDEX; Schema: public; Owner: invest; Tablespace: 
--

CREATE INDEX customer_company_files_447d3092 ON customer_company_files USING btree (company_id);


--
-- TOC entry 2180 (class 1259 OID 207743)
-- Name: customer_company_files_814552b9; Type: INDEX; Schema: public; Owner: invest; Tablespace: 
--

CREATE INDEX customer_company_files_814552b9 ON customer_company_files USING btree (file_id);


--
-- TOC entry 2123 (class 1259 OID 202467)
-- Name: customer_employee_447d3092; Type: INDEX; Schema: public; Owner: invest; Tablespace: 
--

CREATE INDEX customer_employee_447d3092 ON customer_employee USING btree (company_id);


--
-- TOC entry 2124 (class 1259 OID 207610)
-- Name: customer_employee_job_position_id_9ef29403_uniq; Type: INDEX; Schema: public; Owner: invest; Tablespace: 
--

CREATE INDEX customer_employee_job_position_id_9ef29403_uniq ON customer_employee USING btree (job_position_id);


--
-- TOC entry 2138 (class 1259 OID 207708)
-- Name: customer_order_9bea82de; Type: INDEX; Schema: public; Owner: invest; Tablespace: 
--

CREATE INDEX customer_order_9bea82de ON customer_order USING btree (product_id);


--
-- TOC entry 2139 (class 1259 OID 207696)
-- Name: customer_order_cb24373b; Type: INDEX; Schema: public; Owner: invest; Tablespace: 
--

CREATE INDEX customer_order_cb24373b ON customer_order USING btree (customer_id);


--
-- TOC entry 2140 (class 1259 OID 207702)
-- Name: customer_order_d3c2f424; Type: INDEX; Schema: public; Owner: invest; Tablespace: 
--

CREATE INDEX customer_order_d3c2f424 ON customer_order USING btree (order_amount_unit_id);


--
-- TOC entry 2141 (class 1259 OID 207640)
-- Name: customer_order_number_64affdf7_like; Type: INDEX; Schema: public; Owner: invest; Tablespace: 
--

CREATE INDEX customer_order_number_64affdf7_like ON customer_order USING btree (number varchar_pattern_ops);


--
-- TOC entry 2146 (class 1259 OID 207662)
-- Name: customer_orderdiscuss_69dfcb07; Type: INDEX; Schema: public; Owner: invest; Tablespace: 
--

CREATE INDEX customer_orderdiscuss_69dfcb07 ON customer_orderdiscuss USING btree (order_id);


--
-- TOC entry 2161 (class 1259 OID 207660)
-- Name: customer_orderdiscuss_messages_2ace8b8c; Type: INDEX; Schema: public; Owner: invest; Tablespace: 
--

CREATE INDEX customer_orderdiscuss_messages_2ace8b8c ON customer_orderdiscuss_messages USING btree (orderdiscuss_id);


--
-- TOC entry 2162 (class 1259 OID 207661)
-- Name: customer_orderdiscuss_messages_5bb56376; Type: INDEX; Schema: public; Owner: invest; Tablespace: 
--

CREATE INDEX customer_orderdiscuss_messages_5bb56376 ON customer_orderdiscuss_messages USING btree (orderdiscussmessage_id);


--
-- TOC entry 2167 (class 1259 OID 207680)
-- Name: customer_orderdiscuss_order_files_2ace8b8c; Type: INDEX; Schema: public; Owner: invest; Tablespace: 
--

CREATE INDEX customer_orderdiscuss_order_files_2ace8b8c ON customer_orderdiscuss_order_files USING btree (orderdiscuss_id);


--
-- TOC entry 2168 (class 1259 OID 207681)
-- Name: customer_orderdiscuss_order_files_814552b9; Type: INDEX; Schema: public; Owner: invest; Tablespace: 
--

CREATE INDEX customer_orderdiscuss_order_files_814552b9 ON customer_orderdiscuss_order_files USING btree (file_id);


--
-- TOC entry 2175 (class 1259 OID 207694)
-- Name: customer_orderdiscuss_participants_2ace8b8c; Type: INDEX; Schema: public; Owner: invest; Tablespace: 
--

CREATE INDEX customer_orderdiscuss_participants_2ace8b8c ON customer_orderdiscuss_participants USING btree (orderdiscuss_id);


--
-- TOC entry 2176 (class 1259 OID 207695)
-- Name: customer_orderdiscuss_participants_e8701ad4; Type: INDEX; Schema: public; Owner: invest; Tablespace: 
--

CREATE INDEX customer_orderdiscuss_participants_e8701ad4 ON customer_orderdiscuss_participants USING btree (user_id);


--
-- TOC entry 2151 (class 1259 OID 207646)
-- Name: customer_orderdiscussmessage_4f331e2f; Type: INDEX; Schema: public; Owner: invest; Tablespace: 
--

CREATE INDEX customer_orderdiscussmessage_4f331e2f ON customer_orderdiscussmessage USING btree (author_id);


--
-- TOC entry 2127 (class 1259 OID 202499)
-- Name: customer_product_447d3092; Type: INDEX; Schema: public; Owner: invest; Tablespace: 
--

CREATE INDEX customer_product_447d3092 ON customer_product USING btree (company_id);


--
-- TOC entry 2128 (class 1259 OID 202487)
-- Name: customer_product_b583a629; Type: INDEX; Schema: public; Owner: invest; Tablespace: 
--

CREATE INDEX customer_product_b583a629 ON customer_product USING btree (category_id);


--
-- TOC entry 2131 (class 1259 OID 202486)
-- Name: customer_productcategory_6be37982; Type: INDEX; Schema: public; Owner: invest; Tablespace: 
--

CREATE INDEX customer_productcategory_6be37982 ON customer_productcategory USING btree (parent_id);


--
-- TOC entry 2156 (class 1259 OID 207647)
-- Name: customer_unit_name_7802d3e7_like; Type: INDEX; Schema: public; Owner: invest; Tablespace: 
--

CREATE INDEX customer_unit_name_7802d3e7_like ON customer_unit USING btree (name varchar_pattern_ops);


--
-- TOC entry 2108 (class 1259 OID 182277)
-- Name: django_admin_log_417f1b1c; Type: INDEX; Schema: public; Owner: invest; Tablespace: 
--

CREATE INDEX django_admin_log_417f1b1c ON django_admin_log USING btree (content_type_id);


--
-- TOC entry 2109 (class 1259 OID 182278)
-- Name: django_admin_log_e8701ad4; Type: INDEX; Schema: public; Owner: invest; Tablespace: 
--

CREATE INDEX django_admin_log_e8701ad4 ON django_admin_log USING btree (user_id);


--
-- TOC entry 2112 (class 1259 OID 182294)
-- Name: django_session_de54fa62; Type: INDEX; Schema: public; Owner: invest; Tablespace: 
--

CREATE INDEX django_session_de54fa62 ON django_session USING btree (expire_date);


--
-- TOC entry 2115 (class 1259 OID 182295)
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: invest; Tablespace: 
--

CREATE INDEX django_session_session_key_c0390e0f_like ON django_session USING btree (session_key varchar_pattern_ops);


--
-- TOC entry 2116 (class 1259 OID 182313)
-- Name: polls_choice_7aa0f6ee; Type: INDEX; Schema: public; Owner: invest; Tablespace: 
--

CREATE INDEX polls_choice_7aa0f6ee ON polls_choice USING btree (question_id);


--
-- TOC entry 2187 (class 2606 OID 182217)
-- Name: auth_group_permiss_permission_id_84c5c92e_fk_auth_permission_id; Type: FK CONSTRAINT; Schema: public; Owner: invest
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permiss_permission_id_84c5c92e_fk_auth_permission_id FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2186 (class 2606 OID 182212)
-- Name: auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: invest
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2185 (class 2606 OID 182203)
-- Name: auth_permiss_content_type_id_2f476e4b_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: invest
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permiss_content_type_id_2f476e4b_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2189 (class 2606 OID 182232)
-- Name: auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: invest
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2188 (class 2606 OID 182227)
-- Name: auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: invest
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2191 (class 2606 OID 182246)
-- Name: auth_user_user_per_permission_id_1fbb5f2c_fk_auth_permission_id; Type: FK CONSTRAINT; Schema: public; Owner: invest
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_per_permission_id_1fbb5f2c_fk_auth_permission_id FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2190 (class 2606 OID 182241)
-- Name: auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: invest
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2206 (class 2606 OID 207653)
-- Name: c4a66f187e6aa3587c1ef203d1b71715; Type: FK CONSTRAINT; Schema: public; Owner: invest
--

ALTER TABLE ONLY customer_orderdiscuss_messages
    ADD CONSTRAINT c4a66f187e6aa3587c1ef203d1b71715 FOREIGN KEY (orderdiscussmessage_id) REFERENCES customer_orderdiscussmessage(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2196 (class 2606 OID 207611)
-- Name: cus_job_position_id_9ef29403_fk_customer_employeejobposition_id; Type: FK CONSTRAINT; Schema: public; Owner: invest
--

ALTER TABLE ONLY customer_employee
    ADD CONSTRAINT cus_job_position_id_9ef29403_fk_customer_employeejobposition_id FOREIGN KEY (job_position_id) REFERENCES customer_employeejobposition(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2211 (class 2606 OID 207730)
-- Name: customer_company_fil_company_id_bf71faad_fk_customer_company_id; Type: FK CONSTRAINT; Schema: public; Owner: invest
--

ALTER TABLE ONLY customer_company_files
    ADD CONSTRAINT customer_company_fil_company_id_bf71faad_fk_customer_company_id FOREIGN KEY (company_id) REFERENCES customer_company(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2212 (class 2606 OID 207735)
-- Name: customer_company_files_file_id_84d6e141_fk_customer_file_id; Type: FK CONSTRAINT; Schema: public; Owner: invest
--

ALTER TABLE ONLY customer_company_files
    ADD CONSTRAINT customer_company_files_file_id_84d6e141_fk_customer_file_id FOREIGN KEY (file_id) REFERENCES customer_file(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2195 (class 2606 OID 202462)
-- Name: customer_employee_company_id_ec04f21a_fk_customer_company_id; Type: FK CONSTRAINT; Schema: public; Owner: invest
--

ALTER TABLE ONLY customer_employee
    ADD CONSTRAINT customer_employee_company_id_ec04f21a_fk_customer_company_id FOREIGN KEY (company_id) REFERENCES customer_company(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2205 (class 2606 OID 207648)
-- Name: customer_o_orderdiscuss_id_5220e9dd_fk_customer_orderdiscuss_id; Type: FK CONSTRAINT; Schema: public; Owner: invest
--

ALTER TABLE ONLY customer_orderdiscuss_messages
    ADD CONSTRAINT customer_o_orderdiscuss_id_5220e9dd_fk_customer_orderdiscuss_id FOREIGN KEY (orderdiscuss_id) REFERENCES customer_orderdiscuss(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2207 (class 2606 OID 207668)
-- Name: customer_o_orderdiscuss_id_a232fea0_fk_customer_orderdiscuss_id; Type: FK CONSTRAINT; Schema: public; Owner: invest
--

ALTER TABLE ONLY customer_orderdiscuss_order_files
    ADD CONSTRAINT customer_o_orderdiscuss_id_a232fea0_fk_customer_orderdiscuss_id FOREIGN KEY (orderdiscuss_id) REFERENCES customer_orderdiscuss(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2209 (class 2606 OID 207682)
-- Name: customer_o_orderdiscuss_id_cddca1d1_fk_customer_orderdiscuss_id; Type: FK CONSTRAINT; Schema: public; Owner: invest
--

ALTER TABLE ONLY customer_orderdiscuss_participants
    ADD CONSTRAINT customer_o_orderdiscuss_id_cddca1d1_fk_customer_orderdiscuss_id FOREIGN KEY (orderdiscuss_id) REFERENCES customer_orderdiscuss(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2201 (class 2606 OID 207703)
-- Name: customer_orde_order_amount_unit_id_bdf63817_fk_customer_unit_id; Type: FK CONSTRAINT; Schema: public; Owner: invest
--

ALTER TABLE ONLY customer_order
    ADD CONSTRAINT customer_orde_order_amount_unit_id_bdf63817_fk_customer_unit_id FOREIGN KEY (order_amount_unit_id) REFERENCES customer_unit(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2200 (class 2606 OID 207697)
-- Name: customer_order_customer_id_05fff94c_fk_customer_company_id; Type: FK CONSTRAINT; Schema: public; Owner: invest
--

ALTER TABLE ONLY customer_order
    ADD CONSTRAINT customer_order_customer_id_05fff94c_fk_customer_company_id FOREIGN KEY (customer_id) REFERENCES customer_company(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2202 (class 2606 OID 207709)
-- Name: customer_order_product_id_437c1dda_fk_customer_product_id; Type: FK CONSTRAINT; Schema: public; Owner: invest
--

ALTER TABLE ONLY customer_order
    ADD CONSTRAINT customer_order_product_id_437c1dda_fk_customer_product_id FOREIGN KEY (product_id) REFERENCES customer_product(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2208 (class 2606 OID 207673)
-- Name: customer_orderdiscuss_orde_file_id_60c46b0b_fk_customer_file_id; Type: FK CONSTRAINT; Schema: public; Owner: invest
--

ALTER TABLE ONLY customer_orderdiscuss_order_files
    ADD CONSTRAINT customer_orderdiscuss_orde_file_id_60c46b0b_fk_customer_file_id FOREIGN KEY (file_id) REFERENCES customer_file(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2203 (class 2606 OID 207725)
-- Name: customer_orderdiscuss_order_id_6d139333_fk_customer_order_id; Type: FK CONSTRAINT; Schema: public; Owner: invest
--

ALTER TABLE ONLY customer_orderdiscuss
    ADD CONSTRAINT customer_orderdiscuss_order_id_6d139333_fk_customer_order_id FOREIGN KEY (order_id) REFERENCES customer_order(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2210 (class 2606 OID 207687)
-- Name: customer_orderdiscuss_particip_user_id_51d1ce2d_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: invest
--

ALTER TABLE ONLY customer_orderdiscuss_participants
    ADD CONSTRAINT customer_orderdiscuss_particip_user_id_51d1ce2d_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2204 (class 2606 OID 207641)
-- Name: customer_orderdiscussmessage_author_id_49ff33a9_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: invest
--

ALTER TABLE ONLY customer_orderdiscussmessage
    ADD CONSTRAINT customer_orderdiscussmessage_author_id_49ff33a9_fk_auth_user_id FOREIGN KEY (author_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2197 (class 2606 OID 202488)
-- Name: customer_pr_category_id_f9cc3de6_fk_customer_productcategory_id; Type: FK CONSTRAINT; Schema: public; Owner: invest
--

ALTER TABLE ONLY customer_product
    ADD CONSTRAINT customer_pr_category_id_f9cc3de6_fk_customer_productcategory_id FOREIGN KEY (category_id) REFERENCES customer_productcategory(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2199 (class 2606 OID 202481)
-- Name: customer_prod_parent_id_9634f324_fk_customer_productcategory_id; Type: FK CONSTRAINT; Schema: public; Owner: invest
--

ALTER TABLE ONLY customer_productcategory
    ADD CONSTRAINT customer_prod_parent_id_9634f324_fk_customer_productcategory_id FOREIGN KEY (parent_id) REFERENCES customer_productcategory(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2198 (class 2606 OID 202500)
-- Name: customer_product_company_id_2338740b_fk_customer_company_id; Type: FK CONSTRAINT; Schema: public; Owner: invest
--

ALTER TABLE ONLY customer_product
    ADD CONSTRAINT customer_product_company_id_2338740b_fk_customer_company_id FOREIGN KEY (company_id) REFERENCES customer_company(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2192 (class 2606 OID 182267)
-- Name: django_admin_content_type_id_c4bce8eb_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: invest
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_content_type_id_c4bce8eb_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2193 (class 2606 OID 182272)
-- Name: django_admin_log_user_id_c564eba6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: invest
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2194 (class 2606 OID 182314)
-- Name: polls_choice_question_id_c5b4b260_fk_polls_question_id; Type: FK CONSTRAINT; Schema: public; Owner: invest
--

ALTER TABLE ONLY polls_choice
    ADD CONSTRAINT polls_choice_question_id_c5b4b260_fk_polls_question_id FOREIGN KEY (question_id) REFERENCES polls_question(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2381 (class 0 OID 0)
-- Dependencies: 5
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2017-04-03 13:34:13

--
-- PostgreSQL database dump complete
--

