# -*- coding: utf-8 -*-

"""
This module contains form fields to work with.
"""

from django import forms
from django.core.exceptions import ValidationError, FieldError
from django.forms import ClearableFileInput, CharField
from django.forms import CheckboxInput
from django.utils.html import conditional_escape
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _
import re

# Feel free to extend this, see
# http://www.iana.org/assignments/media-types/media-types.xhtml
MEDIA_TYPES = ['image', 'audio', 'video']

"""
<div class="fileinput fileinput-new" data-provides="fileinput">
    <span class="btn btn-default btn-file"><span class="fileinput-new">Select file</span>
    <span class="fileinput-exists">Change</span><input type="file" name="..."/></span>
    <span class="fileinput-filename"></span>
    <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
</div>
"""


class DefaultFileInput(forms.ClearableFileInput):
    initial_text = ''  # _('Currently')
    input_text = _('Change')
    select_text = _('Select file')
    clear_checkbox_label = _('Clear')
    remove_text = '×'

    # template_with_initial = (
    # '%(initial_text)s: <a href="%(initial_url)s">%(initial)s</a> '
    # '%(clear_template)s<br />%(input_text)s: %(input)s'
    # )
    template_with_initial = ("""
    <div class="fileinput fileinput-new" data-provides="fileinput">%(initial_text)s
    <span class="btn btn-default btn-file"><span class="fileinput-new">%(select_text)s</span>
    <span class="fileinput-exists">%(input_text)s</span>%(input)s</span>
    <span class="fileinput-filename">%(initial)s</span>
    <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">%(remove_text)s</a>
    </div> %(clear_template)s
    """)

    template_with_clear = '%(clear)s <label class="control-label" for="%(clear_checkbox_id)s">%(clear_checkbox_label)s</label>'

    def render(self, name, value, attrs=None):
        substitutions = {
            'initial_text': self.initial_text,
            'input_text': self.input_text,
            'select_text': self.select_text,
            'remove_text': self.remove_text,
            'clear_template': '',
            'clear_checkbox_label': self.clear_checkbox_label,
            'initial': '',
            'initial_url': '',

        }
        # template = '%(input)s'
        template = self.template_with_initial
        substitutions['input'] = super(ClearableFileInput, self).render(name, value, attrs)

        if self.is_initial(value):
            substitutions['initial'] = conditional_escape(value)
            substitutions['initial_url'] = conditional_escape(value.url)
            if '/' in substitutions['initial']:
                substitutions['initial'] = re.sub('.*/', '', substitutions['initial'])

        if not self.is_required:
            checkbox_name = self.clear_checkbox_name(name)
            checkbox_id = self.clear_checkbox_id(checkbox_name)
            substitutions['clear_checkbox_name'] = conditional_escape(checkbox_name)
            substitutions['clear_checkbox_id'] = conditional_escape(checkbox_id)
            substitutions['clear'] = CheckboxInput().render(checkbox_name, False,
                                                            attrs={'id': checkbox_id, 'class': 'checkbox i-checks'})
            substitutions['clear_template'] = self.template_with_clear % substitutions

        return mark_safe(template % substitutions)


class DefaultImageInput(DefaultFileInput):
    remove_text = _('Remove')
    template_with_initial = ("""
    <div class="fileupload fileupload-new" data-provides="fileupload">
    <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"></div>
    <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
    <div>
      <span class="btn btn-file"><span class="fileupload-new">%(select_text)s</span><span class="fileupload-exists">%(input_text)s</span>%(input)s</span>
      <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">%(remove_text)s</a>
    </div>
    </div> %(clear_template)s
    """)


class MultiUploadMetaInput(DefaultFileInput):
    """ HTML5 <input> representation. """

    def __init__(self, *args, **kwargs):
        self.multiple = kwargs.pop('multiple', True)
        super(MultiUploadMetaInput, self).__init__(*args, **kwargs)

    def render(self, name, value, attrs=None):
        if self.multiple:
            attrs['multiple'] = 'multiple'

        return super(MultiUploadMetaInput, self).render(name, value, attrs)

    def value_from_datadict(self, data, files, name):
        if hasattr(files, 'getlist'):
            return files.getlist(name)
        else:
            value = files.get(name)
            if isinstance(value, list):
                return value
            else:
                return [value]


class MultiUploadMetaField(forms.FileField):
    """ Base class for the all media types classes. """

    default_error_messages = {
        'min_num': _(
            u'Ensure at least %(min_num)s files are '
            u'uploaded (received %(num_files)s).'),
        'max_num': _(
            u'Ensure at most %(max_num)s files '
            u'are uploaded (received %(num_files)s).'),
        'file_size': _(
            u'File %(uploaded_file_name)s '
            u'exceeded maximum upload size.'),
    }

    def __init__(self, *args, **kwargs):
        self.min_num = kwargs.pop('min_num', 0)
        self.max_num = kwargs.pop('max_num', None)
        self.maximum_file_size = kwargs.pop('max_file_size', None)
        self.widget = MultiUploadMetaInput(
            attrs=kwargs.pop('attrs', {}),
            multiple=(self.max_num is None or self.max_num > 1),
        )
        super(MultiUploadMetaField, self).__init__(*args, **kwargs)

    def to_python(self, data):
        ret = []
        data = data or []
        for item in data:
            i = super(MultiUploadMetaField, self).to_python(item)
            if i:
                ret.append(i)
        return ret

    def validate(self, data):
        super(MultiUploadMetaField, self).validate(data)

        num_files = len(data)
        if num_files and not data[0]:
            num_files = 0

        if not self.required and num_files == 0:
            return

        if num_files < self.min_num:
            raise ValidationError(
                self.error_messages['min_num'] % {
                    'min_num': self.min_num,
                    'num_files': num_files,
                }
            )
        elif self.max_num and num_files > self.max_num:
            raise ValidationError(
                self.error_messages['max_num'] % {
                    'max_num': self.max_num,
                    'num_files': num_files,
                }
            )

        for uploaded_file in data:
            if (self.maximum_file_size and
                        uploaded_file.size > self.maximum_file_size):
                raise ValidationError(
                    self.error_messages['file_size'] % {
                        'uploaded_file_name': uploaded_file.name,
                    }
                )


class MultiFileField(MultiUploadMetaField):
    """ Handles plain files. """
    pass


class MultiMediaField(MultiUploadMetaField):
    """ Handles multimedia files."""

    error_messages = {
        'wrong_type': _(
            u'Invalid media_type. Valid types are: %(valid_types)s')
    }

    def __init__(self, *args, **kwargs):
        self.media_type = kwargs.pop('media_type', 'image')

        if self.media_type not in MEDIA_TYPES:
            raise FieldError(
                self.error_messages['wrong_type'] % {
                    'valid_types': ', '.join(MEDIA_TYPES),
                }
            )

        kwargs.update({
            'attrs': {
                'accept': '{0}/*'.format(self.media_type),
            }
        })
        super(MultiMediaField, self).__init__(*args, **kwargs)


class MultiImageField(MultiMediaField, forms.ImageField):
    """ Handles multiple image uploads, requires Pillow to be installed. """

    def __init__(self, *args, **kwargs):
        kwargs.update({'media_type': 'image'})
        super(MultiImageField, self).__init__(*args, **kwargs)

    def to_python(self, data):
        ret = []
        for item in data:
            i = forms.ImageField.to_python(self, item)
            if i:
                ret.append(i)
        return ret


class CharMaskField(CharField):
    def __init__(self, max_length=None, min_length=None, strip=True, mask=None, *args, **kwargs):
        self.mask = mask
        super(CharMaskField,self).__init__(max_length, min_length, strip, *args, **kwargs)


    def widget_attrs(self, widget):
        attrs = super().widget_attrs(widget)
        if self.mask is not None:
            # The HTML attribute is data-mask.
            attrs['data-mask'] = str(self.mask)
        return attrs
