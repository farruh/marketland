from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext_lazy as _
from account.conf import settings

class AuditDateModel(models.Model):
    created_on = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class AuditUserModel(AuditDateModel):
    """
    @with_author field will add 'author' and 'updated_by' fields to model
    it will be updated every time when model updated by authenticated user
    if user not authenticated it will be None
    """
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_('author'),
                                   related_name="created_%(app_label)s_%(class)s_related",
                                   related_query_name="created_%(app_label)s_%(class)ss", null=True, blank=True,
                                   on_delete=models.SET_NULL, editable=False)

    updated_by = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_('last updated by'),
                                   related_name="updated_%(app_label)s_%(class)s_related",
                                   related_query_name="updated_%(app_label)s_%(class)ss", null=True, blank=True,
                                   on_delete=models.SET_NULL, editable=False)

    class Meta:
        abstract = True

