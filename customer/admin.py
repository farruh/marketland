from django.contrib import admin

from .models import Company, File,Order, Product, ProductCategory, OrderDiscuss, OrderDiscussMessage

admin.site.register(Company)
admin.site.register(File)
admin.site.register(ProductCategory)
admin.site.register(Product)
# admin.site.register(EmployeeJobPosition)
# admin.site.register(Unit)
admin.site.register(Order)
admin.site.register(OrderDiscuss)
admin.site.register(OrderDiscussMessage)

