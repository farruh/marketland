from django.contrib.auth.decorators import login_required

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.core.exceptions import ImproperlyConfigured
from django.urls import reverse_lazy
from django.views import generic
from django.views.generic import CreateView, UpdateView, ListView, FormView
from django.contrib import messages
from django.shortcuts import render
from django.utils.translation import ugettext_lazy as _

from customer.forms import OrderForm, CompanyForm, ProfileForm, ProductForm, ProductFormSet, CompanyFormSet

from customer.models import Company, Employee, Product, Order, OrderDiscuss, OrderDiscussMessage, File, CompanyFile
from mtplus.views import FormsetUpdateView


class IndexView(generic.ListView):
    # template_name = 'index.html'
    context_object_name = 'product_list'
    model = Product
    template_name = 'customer/pub_product_list.html'

    def get_queryset(self):
        # Get list of customers
        search = self.request.GET.get('search')
        if search is not None and search != '':
            return Product.objects.filter(name__icontains=search).order_by('name')
        else:
            return Product.objects.order_by('name')


class ProductDetail(generic.DetailView):
 model = Product
 template_name = 'customer/pub_product_detail.html'

# class DetailView(FormView):
# template_name = 'customer/company_form.html'
# form_class = CustomerForm
# def form_valid(self, form):
# # this method is called when valid form data has been POSTed.
# # it should return an HttpResponse
# return super(DetailView, self).form_valid(form)

class CompanyCreate(FormView):
    form_class = CompanyForm
    template_name = 'customer/company_form.html'
    success_url = reverse_lazy('customer:company_list')


# class CompanyCreate(CreateView):
# model = Company
# fields = ('__all__')
#     success_url = reverse_lazy('customer:company_list')
# template_name = 'customer/company_form.html'


class CompanyUpdate(LoginRequiredMixin, UpdateView):
    model = Company
    fields = ('__all__')
    success_url = reverse_lazy('customer:company_list')
    # template_name = 'customer/company_form.html'


class CompanyList(LoginRequiredMixin, ListView):
    model = Company
    template = 'customer/company_list.html'


class EmployeeUpdate(LoginRequiredMixin, UpdateView):
    model = Employee
    fields = ('__all__')
    success_url = reverse_lazy('customer:employee_list')
    # template_name = 'customer/employee_form.html'


class EmployeeCreate(LoginRequiredMixin, CreateView):
    model = Employee
    fields = ('__all__')
    success_url = reverse_lazy('customer:employee_list')
    # template_name = 'customer/employee_form.html'


class EmployeeList(LoginRequiredMixin, ListView):
    model = Employee
    template_name = 'customer/employee_list.html'


class ProductUpdate(LoginRequiredMixin, FormsetUpdateView):
    form_class = ProductForm
    model = Product
    success_url = reverse_lazy('customer:product_list')
    template_name = 'customer/product_form.html'
    formset_class = ProductFormSet



    #
    # def form_valid(self, form):
    #     cur_company = self.get_object()
    #     print('product id=%s' % cur_company.id)
    #     for each in form.cleaned_data['images']:
    #         ProductImage.objects.create(product=cur_company, image=each)
    #     return super().form_valid(form)


class ProductCreate(ProductUpdate):
    # form_class = ProductForm
    # model = Product
    # success_url = reverse_lazy('customer:product_list')
    # template_name = 'customer/product_form.html'
    # formset_class = ProductFormSet

    def get_object(self):
        if self.request.user.employee:
            product = Product()
            product.company = self.request.user.employee.company
            print('get object %s' % (product.company.name))
            return product
        else:
            raise ImproperlyConfigured(
                "No Employee model associated with user.")


class ProductList(LoginRequiredMixin, ListView):
    model = Product
    template_name = 'customer/product_list.html'


class OrderList(LoginRequiredMixin, ListView):
    model = Order
    template_name = 'customer/order_list.html'


class OrderCreate(LoginRequiredMixin, CreateView):
    form_class = OrderForm
    model = Order
    # fields = ('__all__')
    success_url = reverse_lazy('customer:order_list')
    template_name = 'customer/order_form.html'


class OrderUpdate(LoginRequiredMixin, UpdateView):
    form_class = OrderForm
    model = Order
    # fields = ('__all__')
    success_url = reverse_lazy('customer:order_list')
    template_name = 'customer/order_form.html'


class OrderDiscussList(LoginRequiredMixin, ListView):
    model = OrderDiscuss
    template_name = 'customer/order_discuss_list.html'



def get_field(dict, key):
    try:
        return dict[key]
    except:
        return None


@login_required
def order_discuss(request, pk):
    order_disc = OrderDiscuss.objects.filter(order_id=pk)[0]
    if request.method == 'POST':
        if get_field(request.POST, 'message'):
            order_disc_msg = OrderDiscussMessage(message=get_field(request.POST, 'message'),
                                                 created_by=get_current_user(request))
            order_disc_msg.save()
            order_disc.messages.add(order_disc_msg)
            order_disc.save()
        elif get_field(request.FILES, 'file'):
            f = get_field(request.FILES, 'file')
            file_model = File(name=f.name, file=f)
            file_model.save()
            order_disc.order_files.add(file_model)
            order_disc.save()
    return render(request, 'customer/chat.html',
                  {'order_disc': order_disc, })


def get_current_user(request):
    if request.user.is_authenticated:
        return User.objects.get(username=request.user.username)
    else:
        return None;


def site_index(request, path):
    return render(request, path)


@login_required
def profile_summary(request):
    return render(request, "customer/summary.html")


@login_required
def company_files(request):
    return render(request, "customer/company_files.html")


class ProfileView(LoginRequiredMixin, FormView):
    template_name = "customer/profile.html"
    form_class = ProfileForm
    success_url = reverse_lazy('customer:summary')
    messages = {
        "profile_updated": {
            "level": messages.SUCCESS,
            "text": _("Account profile updated.")
        },
    }

    def get_form_class(self):
        # @@@ django: this is a workaround to not having a dedicated method
        # to initialize self with a request in a known good state (of course
        # this only works with a FormView)
        # self.primary_email_address = EmailAddress.objects.get_primary(self.request.user)
        return super(ProfileView, self).get_form_class()

    def get_initial(self):
        initial = super(ProfileView, self).get_initial()
        initial["last_name"] = self.request.user.last_name
        initial["first_name"] = self.request.user.first_name
        initial["middle_name"] = self.request.user.middle_name
        initial["phone"] = self.request.user.phone
        if hasattr(self.request.user, 'employee'):
            initial["dob"] = self.request.user.employee.dob
            initial["sex"] = self.request.user.employee.sex
            initial["company"] = self.request.user.employee.company
            initial["job_position"] = self.request.user.employee.job_position
            initial["certificates"] = self.request.user.employee.certificates

        return initial

    # def post(self, request, *args, **kwargs): 
    #     form = self.get_form()
    #     if form.is_valid():
    #         print('valid form')
    #         return self.form_valid(form)
    #     else:
    #         print('invalid form, %s\n%s'%(form.cleaned_data,form.errors))
    #         return self.form_invalid(form)     

    def form_valid(self, form):
        print('form valid')
        self.update_profile(form)
        if self.messages.get("profile_updated"):
            messages.add_message(
                self.request,
                self.messages["profile_updated"]["level"],
                self.messages["profile_updated"]["text"]
            )
        # return redirect(self.get_success_url())
        return super(ProfileView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        ctx = super(ProfileView, self).get_context_data(**kwargs)
        return ctx

    def update_profile(self, form):
        self.update_user(form)
        self.update_employee(form)

    def update_user(self, form):
        fields = {}
        if "first_name" in form.cleaned_data:
            fields["first_name"] = form.cleaned_data["first_name"]
        if "last_name" in form.cleaned_data:
            fields["last_name"] = form.cleaned_data["last_name"]
        if "middle_name" in form.cleaned_data:
            fields["middle_name"] = form.cleaned_data["middle_name"]
        if "phone" in form.cleaned_data:
            fields["phone"] = form.cleaned_data["phone"]
        if fields:
            user = self.request.user
            for k, v in fields.items():
                print('setting values %s=%s' % (k, v))
                setattr(user, k, v)
            user.save()

    def update_employee(self, form):
        fields = {}
        if "dob" in form.cleaned_data:
            fields["dob"] = form.cleaned_data["dob"]
        if "sex" in form.cleaned_data:
            fields["sex"] = form.cleaned_data["sex"]
        if "job_position" in form.cleaned_data:
            fields["job_position"] = form.cleaned_data["job_position"]
        if "company" in form.cleaned_data:
            fields["company"] = form.cleaned_data["company"]
        if "certificates" in form.cleaned_data:
            fields["certificates"] = form.cleaned_data["certificates"]
        if fields:
            user = self.request.user
            if hasattr(user, 'employee'):
                employee = user.employee
            else:
                employee = Employee(user = user )
            for k, v in fields.items():
                setattr(employee, k, v)
            employee.save()


            # def get_success_url(self, fallback_url=None, **kwargs):
            # return default_redirect(self.request, fallback_url, **kwargs)


class CompanyView(LoginRequiredMixin, FormsetUpdateView):
    model = Company
    # fields = ('__all__')
    success_url = reverse_lazy('customer:summary')
    template_name = 'customer/company_form.html'
    form_class = CompanyForm
    formset_class = CompanyFormSet
    messages = {
        "company_updated": {
            "level": messages.SUCCESS,
            "text": _("Account company updated.")
        },
    }

    def get_object(self):
        if self.request.user.employee:
            return self.request.user.employee.company
        else:
            raise ImproperlyConfigured(
                "No Employee model associated with user.")

    def forms_valid(self, form, formset):
        # cur_company = self.get_object()
        # print('%s' % cur_company.id)
        # for each in form.cleaned_data['files']:
        #     CompanyFile.objects.create(company=cur_company, file=each)
        valid = super(CompanyView, self).forms_valid(form, formset)
        if self.messages.get("company_updated"):
            messages.add_message(
                self.request,
                self.messages["company_updated"]["level"],
                self.messages["company_updated"]["text"]
            )
        return valid


def product_edit(request, product_id):
    if request.method == 'POST':
        form = ProductForm(request.POST)
        formset = ProductFormSet(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            if formset.is_valid():
                formset.save()


    else:
        product = Product.objects.get(pk=product_id)
        form = ProductForm(instance=product)
        formset = ProductFormSet(instance=product, prefix='formset')
    return render(request, "customer/product_form.html", {'form': form, 'formset': formset})