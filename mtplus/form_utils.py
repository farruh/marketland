from crispy_forms.bootstrap import Tab, TabHolder, FormActions
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Fieldset, Layout, Button, Div, Submit, HTML
from django.utils.translation import ugettext_lazy as _


def get_tab(name, fields):
    fieldSet = Fieldset(None)
    for field in fields:
        if '$$html.' in field:
            field = field.replace('$$html.','')
            fieldSet.fields.append(HTML(field))
        else:
            fieldSet.fields.append(field)

    return Tab(_(name), Div(fieldSet, css_class='panel-body'))


def get_tabholder(tabs):
    tabholder = TabHolder()
    for tab_name, tab_items in tabs.items():
        # print(tab_name)
        tabholder.fields.append(get_tab(tab_name, tab_items))
    return tabholder


def get_helper_hr_tab_form(form_id, tabs):
    helper = FormHelper()
    helper.form_id = form_id

    # horizontal
    helper.form_class = 'form-horizontal'
    helper.label_class = 'col-sm-2'
    helper.field_class = 'col-sm-10'
    # print(tabs)
    # layout
    helper.layout = Layout(
        get_tabholder(tabs),
        FormActions(
            Div(Button('cancel', _('Cancel')), Submit('submit', _('Save changes')), css_class='panel-body')
        )
    )

    return helper


def get_helper_inline_form(form_id, *fields):
    helper = FormHelper()
    helper.form_id = form_id

    # horizontal
    # helper.form_class = 'form-inline'
    helper.form_class = 'm-t'
    helper.field_template = 'bootstrap3/layout/inline_field.html'

    # layout
    helper.layout=Layout()
    helper.layout.extend(fields)

    return helper