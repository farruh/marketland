from django.conf.urls import url

from . import views
from django.conf.urls.static import static
from django.urls import reverse

app_name = 'customer'
urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^summary/$', views.profile_summary, name='summary'),
    url(r'^profile/$', views.ProfileView.as_view(), name='profile'),
    url(r'^files/$', views.company_files, name='files'),

    # url(r'^$', views.index),
    # url(r'^(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='detail'),
    # url(r'^company/list/$', views.CompanyList.as_view(), name='company_list'),
    # url(r'^company/create/$', views.CompanyCreate.as_view(), name='company_create'),
    # url(r'^company/(?P<pk>[0-9]+)/$', views.CompanyUpdate.as_view(), name='company_update'),
    url(r'^company$', views.CompanyView.as_view(), name='company'),


    # employee urls
    url(r'^employee/list/$', views.EmployeeList.as_view(), name='employee_list'),
    url(r'^employee/(?P<pk>[0-9]+)/$', views.EmployeeUpdate.as_view(), name='employee_update'),
    url(r'^employee/create/$', views.EmployeeCreate.as_view(), name='employee_create'),

    # product urls
    url(r'^product/list/$', views.ProductList.as_view(), name='product_list'),
    url(r'^product/(?P<pk>[0-9]+)/$', views.ProductUpdate.as_view(), name='product_update'),
    url(r'^prod/(?P<product_id>[0-9]+)/$', views.product_edit, name='prod_update'),
    url(r'^product/create/$', views.ProductCreate.as_view(), name='product_create'),
    url(r'^product/detail/(?P<pk>[0-9]+)/$', views.ProductDetail.as_view(), name='product_detail'),

    url(r'^order/list/$', views.OrderList.as_view(), name='order_list'),
    url(r'^order/(?P<pk>[0-9]+)/$', views.OrderUpdate.as_view(), name='order_update'),
    url(r'^order/create/$', views.OrderCreate.as_view(), name='order_create'),

    url(r'^order/discuss/list/$', views.OrderDiscussList.as_view(), name='order_discuss_list'),
    url(r'^order/discuss/(?P<pk>[0-9]+)/$', views.order_discuss, name='order_discuss'),


    url(r'^site/(?P<path>.*)$', views.site_index, name='site_index'),


]
