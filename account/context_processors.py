from __future__ import unicode_literals

from django.conf import settings
from account.models import Account


def account(request):
    ctx = {
        "account": Account.for_request(request),
        "ACCOUNT_OPEN_SIGNUP": settings.ACCOUNT_OPEN_SIGNUP,
        "CONTACT_EMAIL": settings.CONTACT_EMAIL,
    }
    return ctx
