from easy_thumbnails.fields import ThumbnailerImageField, ThumbnailerField
from account.compat import is_authenticated
from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User
from django.utils.text import ugettext_lazy as _
from filer.fields.file import FilerFileField
from filer.fields.image import FilerImageField
from audit.models import AuditUserModel
from account.conf import settings

# class CustomerType(enum.Enum): todo: move to separate table
PRODUCER = 'PR'
SERVICE = 'SV'
WHOLESALE = 'WS'
RETAIL = 'RT'
COMPANY_TYPES = (
    (PRODUCER, _("Producer")),
    (SERVICE, _("Service provider")),
    (WHOLESALE, _("Wholesale")),
    (RETAIL, _("Retail")),
)

JOB_POSITIONS = (
    ('ceo', _('CEO')),
    ('slm', _('Sales manager')),
    ('sld', _('Sales director')),
    ('mkm', _('Marketing manager')),
    ('mkd', _('Marketing director')),
    ('fnm', _('Financial manager')),
    ('fnd', _('Financial director')),

)

UNITS = (
    ('kg', _('Kilogram')),
    ('ton', _('Ton')),
    ('cm3', _('cm3')),
    ('m3', _('m3')),
    ('cm2', _('cm2')),
    ('m2', _('m2')),
    ('pc', _('Piece')),
)

COUNTRIES = [('UZB', _('Uzbekistan')), ('RUS', _('Russia'))]

CONTACT_TYPES = (
    ('PT', _('Partner')),
    ('CL', _('Client')),
)

STATUS = (
    ('a', _('Active')),
    ('d', _('Disabled')),
    ('r', _('Removed'))
)
ORDER_STATUS = (
    ('active', _('Active')),
    ('pending', _('Pending')),
    ('canceled', _('Canceled')),
    ('shipped', _('Shipped')),
    ('rejected', _('Rejected')),
)
CURRENCY = (
    ('UZS',_('Uzbek sum')),
    ('USD',_('US dollar')),
    ('EUR',_('Euro')),
    ('RUR',_('Russian ruble')),

    )
# dictionary models todo: add support of i18n

class DictAbstractModel(AuditUserModel):
    name = models.CharField(_('Name'), max_length=100, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        abstract = True


class CompanyType(DictAbstractModel):
    pass


class Unit(DictAbstractModel):
    pass


class JobPosition(DictAbstractModel):
    pass


class ProductCategory(DictAbstractModel):
    parent = models.ForeignKey('self', blank=True, null=True, related_name='children')


class File(DictAbstractModel):
    file = models.FileField(upload_to='uploads/files', verbose_name=_('File'))
    # todo: add hashed title for urls


class Photo(DictAbstractModel):
    file = models.FileField(upload_to='uploads/photos')

    # todo: add hashed title for urls


def custom_directory_path(entity, id, filename):
    return '{0}_{1}/{2}'.format(entity, id, filename)


def company_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return custom_directory_path('company', instance.id, filename)

def company_file_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return custom_directory_path('company', instance.company.id, filename)


def product_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return custom_directory_path('product', instance.product.id, filename)


def employee_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return custom_directory_path('employee', instance.id, filename)


# Business models
class Company(AuditUserModel):
    name = models.CharField(_('Company Name'), max_length=200)
    company_type = models.CharField(_('Company type'), max_length=2, choices=COMPANY_TYPES)
    address = models.CharField(_('Address'), max_length=200)
    phone = models.CharField(_('Phone number'), max_length=100)
    logo = ThumbnailerImageField(_('Logo'), null=True, blank=True, upload_to=company_directory_path)

    bank_name = models.CharField(_('Bank name'), max_length=100)
    bank_account_number = models.CharField(_('Bank Account Number'), max_length=100)
    mfo = models.CharField(_('MFO'), max_length=100)
    inn = models.CharField(_('INN'), max_length=100)
    okonx = models.CharField(_('OKONX'), max_length=100)
    website = models.URLField(_('Website'), max_length=100)
    email = models.EmailField(_('Email'), max_length=100)
    country = models.CharField(_('Country'), max_length=3, choices=COUNTRIES)


    # company_type = models.ForeignKey(CompanyType, verbose_name=_('Company type'))
    foundation_date = models.DateField(_('Foundation date'))
    employees_amount = models.IntegerField(_('Employees amount'))
    production_power = models.IntegerField(_('Production power'))
    notes = models.CharField(_('Notes'), max_length=1024)
    # files = FilerFileField(related_name=_('Files'), blank=True, null=True)
    # files = models.ManyToManyField(File, verbose_name=_('Files'), blank=True, null=True)
    # contacts = models.ManyToManyField(Contact, verbose_name=_('Contacts'), related_name="company")


    def __str__(self):
        return self.name  # ' / '.join({self.company_type, self.name})

    def get_absolute_url(self):
        return reverse('customer:update', kwargs={'pk': self.pk})


class CompanyFile(AuditUserModel):
    company = models.ForeignKey(Company, related_name='files')
    file = ThumbnailerField(_('File'), upload_to=company_file_directory_path)


class Employee(AuditUserModel):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, related_name="employee", verbose_name=_("user"),
                                on_delete=models.CASCADE)  # todo: move user to custom user model
    dob = models.DateField(_('Date of Birth'), blank=True, null=True)
    sex = models.CharField(_('Sex'), max_length=1, choices=(('M', _('Male')), ('F', ('Female'))), blank=True, null=True)

    # job_position = models.ForeignKey(_('Job position'), max_length=3, choices=JOB_POSITIONS)
    job_position = models.CharField(_('Job position'), max_length=3, choices=JOB_POSITIONS)
    certificates = models.CharField(_('Certificates'), max_length=255, blank=True, null=True)
    company = models.ForeignKey(Company, related_name='employee_company', verbose_name=_('Company'))

    photo = ThumbnailerImageField(_('Photo'), null=True, blank=True, upload_to=employee_directory_path)


    def full_name(self):
        return ' '.join({self.user.get_full_name(), self.job_position})

    def __str__(self):
        return self.full_name()

    @classmethod
    def for_request(cls, request):
        user = getattr(request, "user", None)
        if user and is_authenticated(user):
            try:
                return Employee._default_manager.get(user=user)
            except Employee.DoesNotExist:
                pass
        return Employee(user=user)


class Contact(AuditUserModel):
    company = models.ForeignKey(Company, verbose_name=_('Contact company'))
    person = models.ForeignKey(Employee, verbose_name=_('Contact person'))
    contact_type = models.CharField(_('Contact type'), max_length=2, choices=CONTACT_TYPES)


# todo:analyze may be required another model like Service
class Product(AuditUserModel):
    name = models.CharField(_('Name'), max_length=100)
    company = models.ForeignKey(Company, verbose_name=_('Company'))
    category = models.ForeignKey(ProductCategory, verbose_name=_('Category'))
    amount = models.IntegerField(_('Amount'))
    unit = models.CharField(_('Unit'), max_length=3, choices=UNITS)
    power = models.IntegerField(_('Power'))
    min_order = models.IntegerField(_('Minimum order'))
    price = models.FloatField(_('Price'))
    currency = models.CharField(_('Currency'), max_length=3, choices=CURRENCY)
    # unit = models.ForeignKey(Unit,verbose_name=_('Unit'))

    status = models.CharField(_('Status'), max_length=1, choices=STATUS)
    notes = models.CharField(_('Notes'), max_length=1024)
    # images = models.ManyToManyField(ProductImage, verbose_name=_('Images'))

    def __str__(self):
        return self.name


class ProductImage(AuditUserModel):
    image = ThumbnailerImageField(upload_to=product_directory_path)
    product = models.ForeignKey(Product, related_name="images")
    order = models.SmallIntegerField(verbose_name=_('Order'), default=0, blank=False, null=False)


class Order(AuditUserModel):
    number = models.CharField(_('Order number'), max_length=100, unique=True)
    # body = models.CharField(_('Body'),max_length=100)
    product = models.ForeignKey(Product, verbose_name=_('Product'))
    customer = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_('Customer'))
    customer_company = models.ForeignKey(Company, verbose_name=_('Company'))
    order_amount = models.IntegerField(_('Order amount'))
    order_amount_unit = models.CharField(_('Unit'), max_length=3, choices=UNITS)
    # order_amount_unit = models.ForeignKey(Unit,verbose_name=_('Unit'))
    notes = models.CharField(_('Notes'), max_length=1024)
    status = models.CharField(_('Status'), max_length=12, choices=ORDER_STATUS)
    # contacts
    # contact_name = models.CharField(max_length=100)
    # contact_email = models.EmailField(max_length=100)
    # contact_country = models.CharField(max_length=100)
    # contact_phone = models.CharField(max_length=100)
    # contact_website = models.URLField(max_length=100)

    @property
    def name(self):
        return self.number

    def __str__(self):
        return self.name


class OrderDiscussMessage(AuditUserModel):
    message = models.CharField(max_length=200)
    # author = models.ForeignKey(User)  # current user

    def __str__(self):
        return self.message


# @with_author
class OrderDiscuss(AuditUserModel):
    order = models.OneToOneField(Order, verbose_name=_('Order'))
    participants = models.ManyToManyField(settings.AUTH_USER_MODEL)  # discover proper usage
    messages = models.ManyToManyField(OrderDiscussMessage)  # discover proper usage
    order_files = models.ManyToManyField(File)  # order files

    @property
    def name(self):
        return self.id

    def __str__(self):
        return 'discussion for order ' + self.order.number